package pl.intive.javacourse.arrays;

public class ArrayExample {

	public static void main(String[] args) {

		int[] ints = new int[10];

		for (int i = 0; i < ints.length; i++) {
			ints[i] = i + 1;
		}

		for (int i = 0; i < ints.length; i++) {
			if (i % 2 == 1) {
				System.out.println("ints[" + i + "] = " + ints[i]);
			}
		}

		char[] chars = { 'A', 'B', 'C', 'D', 'E', 'F', 'G' };

		for (int i = 0; i < chars.length; i++) {
			System.out.println("chars[" + i + "] = " + chars[i]);
		}
	}

}
