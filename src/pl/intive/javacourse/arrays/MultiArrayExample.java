package pl.intive.javacourse.arrays;

public class MultiArrayExample {

	public static void main(String[] args) {
		int[][] ints = new int[10][10];

		System.out.println("Tablica ints po inicjalizacji");
		for (int i = 0; i < ints.length; i++) {
			for (int j = 0; j < ints[i].length; j++) {
				if (i % 2 == 1 && j % 2 == 1) {
					System.out.println("ints[" + i + "][" + j + "] = " + ints[i][j]);
				}
			}
		}
		
		for (int i = 0; i < ints.length; i++) {
			for (int j = 0; j < ints[i].length; j++) {
				ints[i][j] = i + j + 1;
			}
		}

		System.out.println("Tablica ints po wypełnieniu danymi");
		for (int i = 0; i < ints.length; i++) {
			for (int j = 0; j < ints[i].length; j++) {
				if (i % 2 == 1 && j % 2 == 1) {
					System.out.println("ints[" + i + "][" + j + "] = " + ints[i][j]);
				}
			}
		}

		ints = new int[5][];
		for (int i = 0; i < ints.length; i++) {
			ints[i] = new int[i+1];
			for (int j = 0; j < ints[i].length; j++) {
				ints[i][j] = j + 1;
			}
		}

		System.out.println("Tablica ints po wypełnieniu danymi z różna liczbą wierszy");
		for (int i = 0; i < ints.length; i++) {
			for (int j = 0; j < ints[i].length; j++) {
				System.out.println("ints[" + i + "][" + j + "] = " + ints[i][j]);
			}
		}

		String[][] strings = { { "A", "B", "C", "D", "E", "F", "G" }, { "B", "C", "D", "E", "F", "G", "H", "I", "J" },
				{ "C", "D", "E" } };

		System.out.println("Inicjalizowana statycznie tablica stringów");
		for (int i = 0; i < strings.length; i++) {
			for (int j = 0; j < strings[i].length; j++) {
				System.out.println("strings[" + i + "][" + j + "] = " + strings[i][j]);
			}
		}
	}
}
