package pl.intive.javacourse.operators;

public class OperatorsExample {

	public static void main(String[] args) {
		int i = 1;
		int j = 2;

		System.out.println(" i == 1 -> " + (i == 1));
		System.out.println(" i == 2 -> " + (i == 2));
		System.out.println(" i != 2 -> " + (i != 2));

		System.out.println(" j > 2 -> " + (j > 2));
		System.out.println(" j >= 2 -> " + (j >= 2));
		System.out.println(" j < 2 -> " + (j < 2));
		System.out.println(" j <= 2 -> " + (j <= 2));
	}
}
