package pl.intive.javacourse.variables;

import java.util.Arrays;

public class ReferencesExample {

	public static void main(String[] args) {
		// Zmienne - dla typów prostych
		int n = 10;
		int m = n;
		System.out.println("n = " + n + ", m = " + m);
		
		// Zmiana dotyczy tylko jednej zmiennej
		n = 5;
		System.out.println("n = " + n + ", m = " + m);
		
		m = 15;
		System.out.println("n = " + n + ", m = " + m);
				
		
		// Zmienne, refencje - dla typów złożonych, obiektów
		int[] a1 = {1, 2, 3};
		int[] a2 = a1;
		System.out.println("Tablica a1 = " + Arrays.toString(a1));
		System.out.println("Tablica a2 = " + Arrays.toString(a2));

		// Zmianę widzą wszystie referencje.
		a1[0] = a1[1] = a1[2] = 0;
		System.out.println("Tablica a1 = " + Arrays.toString(a1));
		System.out.println("Tablica a2 = " + Arrays.toString(a2));
		
		a2[0] = a2[1] = a2[2] = 1;
		// zmiana referencji
		a2 = new int[5];
		for (int i = 0; i < a2.length; i++) {
			a2[i] = (i + 1) * (i + 1); 
		}
		
		System.out.println("Tablica a1 = " + Arrays.toString(a1));
		System.out.println("Tablica a2 = " + Arrays.toString(a2));
	}
}