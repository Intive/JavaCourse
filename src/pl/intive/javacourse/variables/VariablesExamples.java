package pl.intive.javacourse.variables;

public class VariablesExamples {

	public static void main(String[] args) {

		int x = 5;
		int y = 10;

		int result = x * y;

		String message = "Wynik równania to: ";

		System.out.println("x = " + x);
		System.out.println("y = " + y);
		System.out.println(message + "x * y = " + result);
		System.out.println();

		double a = 5.0, b = 0.5;

		System.out.println("a = " + a);
		System.out.println("b = " + b);
		String output = message + "a * b = " + a * b;

		System.out.println(output);

	}

}
