package pl.intive.javacourse.loops;

import pl.intive.javacourse.utils.Sleep;

public class InfiniteForLoopExample {

	public static void main(String[] args) {

		for (;;) {
			System.out.println("Ta pętla nigdy sie nie skończy...");
			Sleep.of(2).seconds();
		}

	}

}
