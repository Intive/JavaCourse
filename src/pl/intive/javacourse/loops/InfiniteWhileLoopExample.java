package pl.intive.javacourse.loops;

import pl.intive.javacourse.utils.Sleep;

public class InfiniteWhileLoopExample {

	public static void main(String[] args) {

		int i = 3;

		while (true) {
			if (i-- == 0) {
				break;
			}

			System.out.println("Ta pętla nigdy się nie skończy...");
			Sleep.of(2).seconds();

		}

		System.out.println("...chyba że zostanie zakończona za pomocą break.");

	}

}
