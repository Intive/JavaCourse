package pl.intive.javacourse.loops;

public class WhilePrefixDecrementExample {

	public static void main(String[] args) {

		System.out.println("Dekrementacja licznika pętli z operatorem prefixowym: ");

		int j = 10;
		while (--j > 0) {
			System.out.println("j = " + j);
		}
		System.out.println("Poza pętlą: j = " + j);

	}

}
