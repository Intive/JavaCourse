
package pl.intive.javacourse.loops;

public class NestedLoopBreak {

	public static void main(String[] args) {

		System.out.println("Wychodzenie z zagnieżdzonej pœtli bez etykiety: ");
		int r = 10, s = 10;
		while (--r > 0) {
			s = 10;
			while (--s > 0) {
				if (r == 5 || s == 5) {
					break;
				}
				System.out.println("r = " + r + ", s = " + s);
			}
		}
		System.out.println("Poza pętlą: r = " + r + ", s = " + s);
		System.out.println();

		System.out.println("Wychodzenie z zagnieżdzonej pętli z etykietą: ");
		int o = 10, p = 10;
		out: while (--o > 0) {
			p = 10;
			while (--p > 0) {
				if (o == 5 && p == 5) {
					break out;
				}
				System.out.println("o = " + o + ", p = " + p);
			}
		}
		System.out.println("Poza pętla: o = " + o + ", p = " + p);

	}

}
