package pl.intive.javacourse.loops;

public class ContinueExample {

	public static void main(String[] args) {

		System.out.println("Przykład użycia continue: ");

		int l = 10;
		while (--l > 0) {
			if (l < 5) {
				continue;
			}
			System.out.println("l = " + l);
		}

		System.out.println("Poza pętlą: l = " + l);

	}

}
