package pl.intive.javacourse.loops;

public class ForExample {

	public static void main(String[] args) {

		System.out.println("Przykład pętli for");
		for (int i = 0; i < 3; i++) {
			System.out.println("i = " + i);
		}

		System.out.println("Odpowiednik pętli z użyciem while:");
		int j = 0;
		while (j < 3) {
			System.out.println("j = " + j);
			j++;
		}

		System.out.println("Nie wszystkie bloki muszą być uzupełnione:");
		int k = 0;
		for (; k < 3;) {
			System.out.println("k = " + k);
			k++;
		}
	}

}
