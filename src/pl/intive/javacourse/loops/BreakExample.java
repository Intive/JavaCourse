package pl.intive.javacourse.loops;

public class BreakExample {

	public static void main(String[] args) {

		System.out.println("Przykład użycia break: ");

		int k = 10;
		while (true) {
			System.out.println("k = " + k);
			if (k-- == 0) {
				break;
			}
		}

		System.out.println("Poza pętlą: k = " + k);
	}

}
