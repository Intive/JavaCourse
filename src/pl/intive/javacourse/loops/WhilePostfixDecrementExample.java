package pl.intive.javacourse.loops;

public class WhilePostfixDecrementExample {

	public static void main(String[] args) {

		System.out.println("Dekrementacja licznika pętli z operatorem postfixowym: ");

		int i = 10;
		while (i-- > 0) {
			System.out.println("i = " + i);
		}
		System.out.println("Poza pętlą: i = " + i);

	}

}
