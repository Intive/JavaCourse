package pl.intive.javacourse.loops;

public class DoWhileExample {

	public static void main(String[] args) {

		System.out.println("Przykład pętli do... while");
		int i = 5;
		do {
			System.out.println("i = " + i--);
		} while (i > 0);

		System.out.println("Pętla zostanie wykonana chociaż raz nawet gdy warunek jest niespełniony:");
		do {
			System.out.println("i = " + i--);
		} while (i > 0);

	}

}
