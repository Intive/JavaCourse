package pl.intive.javacourse.loops;

public class NestedWhileExample {

	public static void main(String[] args) {

		System.out.println("Przykład zagnieżdzonych pętli: ");

		int o = 10, p = 10;
		while (--o > 0) {
			p = 10;
			while (--p > 0) {
				System.out.println("o = " + o + ", p = " + p);
			}
		}

		System.out.println("Poza pętlą: o = " + o + ", p = " + p);

	}

}
