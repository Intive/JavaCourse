package pl.intive.javacourse.utils;

public class Sleep {

	private long unit;

	private Sleep(Long unit) {
		this.unit = unit;
	}

	public static Sleep of(long units) {
		return new Sleep(units);
	}

	public static Sleep of(int units) {
		return of((long) units);
	}

	public void seconds() {
		delay(unit * 1000);
	}

	public void milliseconds() {
		delay(unit);
	}

	void delay(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw new RuntimeException("Thread interrupted");
		}
	}

}
