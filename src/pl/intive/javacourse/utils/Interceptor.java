package pl.intive.javacourse.utils;

import java.io.OutputStream;
import java.io.PrintStream;

class Interceptor extends PrintStream {

	StringBuilder sb = new StringBuilder();

	public Interceptor(OutputStream out) {
		super(out, true);
	}

	@Override
	public void print(String s) {
		sb.append(s);
		super.print(s);
	}

	public String getOutput() {
		return sb.toString();
	}
}