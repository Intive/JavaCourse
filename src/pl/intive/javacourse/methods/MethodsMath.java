package pl.intive.javacourse.methods;

public class MethodsMath {

	
	public static double square(double a) {
		return a * a;
	}

	/**
	 * Oblicza odleglość od punktu P1(x1, y1) do punktu P2(x2, y2) używając wzoru euklidesa.
	 * 
	 * @param x1 Odcięta punktu P1.
	 * @param y1 Rzędna punktu P1.
	 * @param x2 Odcięta punktu P1.
	 * @param y2 Rzędna punktu P2.
	 * @return Odleglość od punktu P1 do punktu P2.
	 */
	public static double distance(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}
	
	public static void printDistance(double x1, double y1, double x2, double y2) {
		double distance = distance(x1, y1, x2, y2);
		System.out.println("Odleglość od punktu (" + x1 + ", " + y1 + ") do punktu (" + x2 + ", " + y2 + ") wynosi: " + distance);
		System.out.println(String.format("Odleglość od punktu (%.3f, %.3f) do punktu (%.3f, %.3f) wynosi: %.3f", x1, y1, x2, y2, distance));
	}
	
	public static void main(String[] args) {
		printDistance(0.0, 0.0, 1.0, 0.0);
		printDistance(0.0, 0.0, 0.0, 1.0);
		printDistance(0.0, 0.0, 1.0, 1.0);
	}
}