package pl.intive.javacourse.methods;

public class MethodsPrintArray {

	public static void main(String[] args) {
		int[] t = {0, 1, 5, 10};
		
		for (int i = 0; i < t.length; i++) {
			System.out.println("t[" + i + "]=" + t[i]);
		}
		System.out.println();
		
		for (int i = 0; i < t.length; i++) {
			t[i] *= 2;
		}

		for (int i = 0; i < t.length; i++) {
			System.out.println("t[" + i + "]=" + t[i]);
		}
		System.out.println();
	}
}