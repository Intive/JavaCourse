package pl.intive.javacourse.methods;

public class MethodMain {

	public static void main(String[] args) {
		System.out.println("Program został wywołany z " + args.length + " argumentami.");
		for (int i = 0; i < args.length; i++) {
			System.out.println("Argument " + (i + 1) + " = " + args[i]);
		}
	}
}
