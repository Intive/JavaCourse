package pl.intive.javacourse.methods;

import java.util.Arrays;

public class MethodsExample {

	public static void printSomething() {
		// metoda bez argumentow i nic nie zwraca
		System.out.println("Cos");
	}

	public static void printMsg(String msg) {
		// metoda bez argumentow i nic nie zwraca
		System.out.println(msg);
	}

	public static String upperCase(String msg) {
		// msg jest lokalna dla metody upperCase
		msg = msg.toUpperCase();
		return msg;
	}

	public static double square(double a) {
		return a * a;
	}
	
	public static void doNothing(int n) {
		n = 15;
	}

	public static void clearArray(int[] a) {
		for (int i = 0; i < a.length; i++) {
			a[i] = 0;
		}
	}

	public static int max(int x, int y, int z) {
		// lokalna zmianna max dla metody max
		int max = x > y ? x : y;
		return max > z ? max : z;
	}

	public static void main(String[] args) {
		printSomething();
		
		int a = 3; 
		int b = 2;
		int c = 5;
		
		// lokalna zmienna max dla metody main
		int max = 0;
		max = max(a, b, c);
		printMsg("max = " + max);
		
		// msg jest lokalna dla metody main
		String msg = "Adam ma laptopa Dell, Intel® Core™ i7, 8MB RAM, 15.6 cali";
		printMsg("upperCase(msg) = " + upperCase(msg));
		printMsg("msg = " + msg);
		
		int n = 10;
		printMsg("n = " + n);
		doNothing(n);
		printMsg("n = " + n);
		
		int array[] = {0, 1, 10, 100, 1000};
		printMsg(Arrays.toString(array));
		clearArray(array);
		printMsg(Arrays.toString(array));
	}
}