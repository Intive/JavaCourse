package pl.intive.javacourse.statements;

public class BooleansEvaluationExample {

	public static void main(String[] args) {

		System.out.println("Jeżeli użyjemy operatora || obydwa wyrażenia muszą by fałszywe, żeby wyrażenie było fałszywe:");
		System.out.println("false || true = " + (false || true));
		System.out.println("Jeżeli użyjemy operatora && obydwa wyrażenia muszą by prawdziwe, żeby wyrażenie było fałszywe:");
		System.out.println("false && true = " + (false && true));
		System.out.println("Całe wyrażenie może zostać zanegowane:");
		System.out.println("!(false && true)= " + !(false && true));
		System.out.println("Zanegowane trueStatement:");
		System.out.println("!true = " + !true);
	}
}
