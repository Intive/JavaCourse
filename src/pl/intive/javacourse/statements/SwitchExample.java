package pl.intive.javacourse.statements;

import java.util.Scanner;

public class SwitchExample {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Podaj swoje imię:");

		String name = scanner.nextLine();

		String greeting = "Witaj nieznajomy";

		switch (name) {
		case "Jurek":
			greeting = "Witam pana prezesa.";
			break;
		case "Beata":
			greeting = "Witam panią dyrektor.";
			break;
		default:
			greeting = "Witam Cię, " + name + ".";

		}

		System.out.println(greeting);

		scanner.close();

	}
}
