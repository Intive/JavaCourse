package pl.intive.javacourse.statements;

import java.util.Arrays;

public class Reference {

    public static void main(String[] args) {
        // Przykłady instrukcji dla typów prymitywnych
        int i1 = 1;
        int i2 = i1;
        System.out.println("i1 = " + i1 + ", i2 = " + i2);
        // Zmiana tylko i1
        i1++;
        System.out.println("i1 = " + i1 + ", i2 = " + i2);
    
        // Przykłady instrukcji dla referencji
        String s1 = "Wojtek ma Lenovo";
        String s2 = ""; 
        System.out.println("s1 = " + s1 + ", s2 = " + s2);
        s2 = s1;
        System.out.println("s1 = " + s1 + ", s2 = " + s2);
        // Obiekt klasy String jest niezmienny.
        // Poniższa instrukcja tworzy nowy obiekt.
        s1 = s1.replace("ma", "nie ma");
        System.out.println("s1 = " + s1 + ", s2 = " + s2);
        
        // Poniżej też są referencje.
        int[] t1 = new int[5];  
        int[] t2 = t1;
        System.out.println("t1 = " + Arrays.toString(t1) + ", t2 = " + Arrays.toString(t2));
        // Tablice można zmieniać. Uwaga: zmienia się zarówno t1[0] jak i t2[0].
        t1[0] = 1;
        System.out.println("t1 = " + Arrays.toString(t1) + ", t2 = " + Arrays.toString(t2));
        
        t1 = new int[2]; 
        System.out.println("t1 = " + Arrays.toString(t1) + ", t2 = " + Arrays.toString(t2));
        t1[0] = 2;
        System.out.println("t1 = " + Arrays.toString(t1) + ", t2 = " + Arrays.toString(t2));
        
        // Pusta referencja
        t2 = null;
        System.out.println("t1 = " + Arrays.toString(t1) + ", t2 = " + Arrays.toString(t2));
        // t2.length - instrukcja spowoduje błąd
        if (t1 != null) {
            System.out.println("Długość t1 = " + t1.length);
        } else {
            System.out.println("t1 nie odwołuje się do żadnej tablicy");
        }
        // t2.length - instrukcja spowoduje błąd
        if (t2 != null) {
            System.out.println("Długość t2 = " + t2.length);
        } else {
            System.out.println("t2 nie odwołuje się do żadnej tablicy");
        }
    }
}
