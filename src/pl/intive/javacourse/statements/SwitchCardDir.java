package pl.intive.javacourse.statements;

public class SwitchCardDir {

	public static void main(String[] args) {
		char ch = 'N';

		String desc;
		switch (ch) {
		case 'E':
			desc = "wschód";
			break;
		case 'W':
			desc = "zachód";
			break;
		case 'N':
			desc = "północ";
			break;
		case 'S':
			desc = "południe";
			break;
		default:
			desc = "nieznany";
		}

		System.out.println("Wybrano kierunek: " + desc);
	}
}