package pl.intive.javacourse.statements;

public class IfCardDir {

	public static void main(String[] args) {
		char ch = 'N';

		String desc;
		if (ch == 'E') {
			desc = "wschód";
		} else if (ch == 'W') {
			desc = "zachód";
		} else if (ch == 'N') {
			desc = "północ";
		} else if (ch == 'S') {
			desc = "południe";
		} else {
			desc = "nieznany";
		}

		System.out.println("Wybrano kierunek: " + desc);
	}
}
