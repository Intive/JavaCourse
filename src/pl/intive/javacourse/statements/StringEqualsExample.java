package pl.intive.javacourse.statements;

public class StringEqualsExample {

	public static void main(String[] args) {
		String name = new String("Staszek");
		String diffrentName = new String("Staszek");

		System.out.println("name == diffrentName: " + (name == diffrentName));
		System.out.println("name.equals(diffrentName): " + name.equals(diffrentName));

	}

}
