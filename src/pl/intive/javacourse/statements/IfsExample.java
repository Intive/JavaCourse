package pl.intive.javacourse.statements;

import java.util.Scanner;

public class IfsExample {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int i = scanner.nextInt();

		System.out.println("Przykład id");

		if (i % 2 == 0) {
			System.out.println("i jest parzyste");
		}

		System.out.println("Przykład if...else");

		if (i > 10) {
			System.out.println("i > 10");
		} else {
			System.out.println("i <= 10");
		}

		System.out.println("Przykład if...else");

		if (i > 10) {
			System.out.println("i > 10");
		} else if (i > 5) {
			System.out.println("i > 5 oraz i <= 10");
		} else if (i >= 0) {
			System.out.println("i <= 5 && i > 0");
		} else {
			System.out.println("i jest ujemne");
		}

		System.out.println("W wyrażeniu można używac bezpośrednio zmiennych logicznych.");
		boolean isPositive = i > 0;

		boolean isDisivibleBy5 = i % 5 == 0;

		if (isPositive) {
			System.out.println("i jest dodatnie");
		}

		if (isPositive && isDisivibleBy5) {
			System.out.println("i jest dodatnie i podzielne przez 5");
		}

		scanner.close();

	}

}
