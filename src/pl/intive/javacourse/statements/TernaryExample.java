package pl.intive.javacourse.statements;

import java.util.Scanner;

public class TernaryExample {

	public static void main(String[] args) {

		System.out.println("Przykład operatora trójargumentego");

		Scanner scanner = new Scanner(System.in);
		int i = scanner.nextInt();

		String isEvenOrOdd = i % 2 == 0 ? "parzysta" : "nieparzysta";

		System.out.println(isEvenOrOdd);

		scanner.close();

	}

}
