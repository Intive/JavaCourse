package pl.intive.javacourse.statements;

public class IfsBlockExample {

	public static void main(String[] args) {

		boolean condition = true;

		if (condition) {
			System.out.println("Po if można użyć nawiasów klamrowych ...");
		}

		if (condition)
			System.out.println("...ale nie jest to wymagane");

		condition = false;

		if (condition)
			System.out.println("Wyrażenie 'codition' jest fałszywe, więc nie powinno się wykonać ...");
		System.out.println(
				"... ups, jeżeli nie ma nawiasów klamrowych, to if działa tylko na wyrażenie bezpośrednio za nim.");

		if (condition) {
			System.out.println("Zostały użyte nawiasy klamrowe ...");
			System.out.println("... żadna z tych instrukcji się nie wykona.");
		}

	}

}
