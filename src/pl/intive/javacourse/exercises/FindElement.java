package pl.intive.javacourse.exercises;

import java.util.Scanner;

/**
 * Napisz program sprawdzający czy w tablicy jest komórka z wpisanym numerem.<br/> 
 * 
 * Na standardowe wejście przyjmij liczbę całkowitą.<br/>
 *
 * Na standardowym wyjściu wypisz wiersz i kolumnę komórki, której wartość równa jest wczytanej liczbie 
 * lub "Nie znaleziono", jeśli nie ma w tablicy takiej komórki.<br/>
 * Założenie: każda komórka w tablicy ma inną wartość.
 * 
 */
public class FindElement {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt();
		sc.close();

		int[][] a = new int[10][10];
		int n = 1;
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = n++;
			}
		}
		
		// TODO znajdź liczbę w tablicy

		System.out.println("Nie znaleziono");
	}
}
