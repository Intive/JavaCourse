package pl.intive.javacourse.exercises;

/**
 * Zrób program dodający dwie macierze i wyświetlajacy wynik.<br/>
 * <code>
 * 1 3 5     5 2 1     6 5 6<br/>
 * 1 3 5  +  6 2 1  =  7 5 6<br/>
 * 1 7 5     2 1 1     3 8 6<br/>
 * </code>
 */
public class Macierz {

	public static void main(String[] args) {

		int[][] macierz = new int[][] { 
			new int[] { 1, 3, 5 },
			new int[] { 1, 3, 5 },
			new int[] { 1, 7, 5 } 
		};

		for (int i = 0; i < macierz.length; i++) {
			int[] wiersz = macierz[i];
			for (int j = 0; j < wiersz.length; j++) {
				System.out.print(wiersz[j]);
				if (j != wiersz.length - 1) {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		int[][] macierz2 = new int[][] { 
			new int[] { 5, 2, 8 },
			new int[] { 7, 4, 1 },
			new int[] { 1, 6, 5 } 
		};
		System.out.println();
		for (int i = 0; i < macierz2.length; i++) {
			int[] wiersz = macierz2[i];
			for (int j = 0; j < wiersz.length; j++) {
				System.out.print(wiersz[j]);
				if (j != wiersz.length - 1) {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		System.out.println();
		int[][] wynik = new int[3][3];	
		
	for (int i = 0; i < wynik.length; i++) {
		for (int j = 0; j < wynik.length; j++) {
			System.out.print(wynik[i][j]);
			if (j != wynik.length - 1) {
				System.out.print(" ");
			}
		}
		System.out.println();	
	}
	System.out.println();
	for (int i = 0; i < wynik.length; i++) {
		for (int j = 0; j < wynik.length; j++) {
			wynik[i][j] = macierz[i][j] + macierz2[i][j];
			System.out.print(wynik[i][j]);
			if (j != wynik.length - 1) {
				System.out.print(" ");
			}
		}
		System.out.println();	
	}
}
}

	
