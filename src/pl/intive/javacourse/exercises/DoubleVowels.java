package pl.intive.javacourse.exercises;

/**
 * Napisz program sprawdzający zwracający liczbę samogłosek bezpośrednio po których również następuje samogłoska.
 * Np.: 
 * <ul>
 * <li>"jakiś tekst" - 0 podwójnych,</li>
 * <li>"Niedźwiedź" - 2 podwójne,</li>
 * <li>"maoam" - 2 podwójne.</li>
 * <br/>

 * Na standardowe wejście przyjmij wyraz s.<br/>
 *
 * Na standardowym wyjściu liczbę samogłosek.<br/>
 * 
 * Uwaga: Napis można skonwertować na tablicę za pomocą metody toCharArray():
 * <code>
 *   char[] a = s.toCharArray();
 * </code>
 */
public class DoubleVowels {

	public static void main(String[] args) {
		
	}
}
