package pl.intive.javacourse.exercises;

import java.util.Random;
import java.util.Scanner;

public class Game {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int userChoose = -1;
		do {
			System.out.println("Wybierz przedmiot, kamien(0), nozyce(1), papier(2). Koniec(-1): ");
			userChoose = scanner.nextInt();

			if (userChoose != -1) {
				Random r = new Random();
				int computerChoose = r.nextInt(3);
				final int KAMIEN = 0;
				final int NOZYCE = 1;
				final int PAPIER = 2;
	
				String computerChooseTxt;
				switch (computerChoose) {
				case KAMIEN:
					computerChooseTxt = "kamien";
					break;
				case NOZYCE:
					computerChooseTxt = "nozyce";
					break;
				default:
					computerChooseTxt = "papier";
				}
				System.out.println("Komputer wybral " + computerChooseTxt);
	
				if (userChoose == computerChoose) {
					System.out.println("Remis");
				} else {
					boolean userWin = true;
					switch (userChoose) {
					case KAMIEN:
						if (computerChoose == NOZYCE)
							userWin = true;
						else
							userWin = false;
						break;
					case NOZYCE:
						if (computerChoose == PAPIER)
							userWin = true;
						else
							userWin = false;
						break;
					case PAPIER:
						if (computerChoose == KAMIEN)
							userWin = true;
						else
							userWin = false;
						break;
					}
					if (userWin) {
						System.out.println("Wygrywasz");
					} else {
						System.out.println("Przegrywasz");
					}
				}
			}
			System.out.println();
		} while (userChoose != -1);
		
		System.out.println("Koniec gry");
	}
}
