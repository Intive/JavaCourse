package pl.intive.javacourse.exercises;

/**
 * Napisz program rysujący trójkąt równoramienny o zadanej wysokości.
 * Na standardowe wejście przyjmij liczbę naturalną n.
 * Na standardowym wyjściu wypisz gwiadki w n wierszach, w liczbie:
 * - 1 w pierszym wierszu, 
 * - 3 w drugim wierszu, ...  i (2*(n-1) + 1) w n-tym wierszu *.
 * Przy czym gwiazdki powinny być wypisywane tak by trójką był symetryczny względem środkowej kolumny, 
 * a ostatnia kolumna zaczynała się od gwazdki.
 * 
 * Na przykład dla n = 3
 *   *
 *  ***
 * *****
 */
public class Triangle2 {

}
