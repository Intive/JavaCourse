package pl.intive.javacourse.exercises;

/**
 * Napisz program sprawdzający czy napis jest palindromem. 
 * <blockquote cite="https://pl.wikipedia.org/wiki/Palindrom">Palindrom to wyrażenie brzmiące tak samo czytane od lewej do prawej i od prawej do lewej</blockquote>.<br/>
 * 
 * Na standardowe wejście przyjmij wyraz s.<br/>
 *
 * Na standardowym wyjściu wypisz czy wyraz jest palindromem.<br/>
 * 
 * Uwaga: Napis można skonwertować na tablicę za pomocą metody toCharArray():
 * <code>
 *   char[] a = s.toCharArray();
 * </code>
 */
public class Palindrome {

	public static void main(String[] args) {
		
	}
}
