package pl.intive.javacourse.exercises;           ///REKURENCJA - METODY REKURENCYJUNE

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class RecursiveFiles {

	public static void main(String[] args) {
		
		 Path path = Paths.get(".");          // ścieżka = pobierz ścieżkę
		 File file = path.toFile();           // plik = pobierz plik ze ścieżki
		 showFiles(file,0);                   // stworzona metoda - pokażŚcieżkę

		
	}

	static void showFiles(File folder, int level){   //tworzymy metodę - pokażŚciezkę
		File[] children = folder.listFiles();        //tablica children = funkcja wylistuj pliki z folderu
		
		for (int i = 0; i < children.length; i++) { // normalny for
			System.out.print(createLine(level)+children[i].getName()); //metoda + tablica oraz funkcję zwracającą nazwę
			if (children[i].isDirectory()) {                        // warunek - jeżeli prawda to (korzystamy z funkcji is.Directory)- sysoutprntln " - Katalog"
				System.out.println(" - Katalog");
				showFiles(children[i],level+1);                     //pokażŚcieżkę
			} else {
				System.out.println(" - Plik");
			}
		}
	}

	static String createLine(int length) {               //metoda dodająca linie/kreski (int oznaczający długość)
 
		String str = "|";
		for (int i = 0; i < length; i++) {               //normalny for
			str = str + "-";                             //dodajemy kreski do str
		}
		return str;                                      //zwracamy stringa
	}
}
