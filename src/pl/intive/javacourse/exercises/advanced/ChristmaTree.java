package pl.intive.javacourse.exercises.advanced;

/**
 * Napisz program, który rysuje chionkę o zadanym rozmiarze.<br/>
 * Ze standardowego wejścia wczytaj rozmiar choinki n.<br/>
 * Na standardowym wyjściu narysuj choinkę, która skłąda się z n części.
 * Części są rosowane od góry do dołu od najmniejszej do największej.
 * <ul>
 * <li>Najmniejsza (pierwsza) część składa się z jednego wiersza, z jedną gwiadką.</li>
 * <li>Druga część składa się z dwóch wierszy - pierwszy z jednej gwiazdki, drugi z 3 gwiazdek.</li>
 * <li>...</li>
 * <li>Największa - n-ta część - składa się z n wierszy, gdzie pierwszy ma jedną gwiazdkę, drugi 3 gwiazdki, a n-ty 2 * n - 1 gwazdek.</li>
 * </ul>
 * Każdy wiersz powinien być wyśrodkowany, za pomocą spacji, tak by choinka była symetryczna względem n-tej kolumny i by najbardziej skrajna gwazdka z lewej strony choinki była w pierwszym rzędzie. Można przyjąć że n <= 20 <br/>
 * Przykładowa choinka dla n = 3:<br/>
 * &nbsp;&nbsp;*<br/>
 * &nbsp;&nbsp;*<br/>
 * &nbsp;***<br/>
 * &nbsp;&nbsp;*<br/>
 * &nbsp;***<br/>
 * *****<br/>
 */
public class ChristmaTree {

	
	
}
