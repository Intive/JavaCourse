package pl.intive.javacourse.exercises.advanced;

/**
 * Napisz funkcję o nazwie extract, która na wejście przyjmie String zawierającą numer PESEL, oraz zwróci
 * tablicę zawierającą:
 * 
 * - "p" gdy PESEL jest prawidłowy, lub "f" gdy jest nieprawidłowy
 * - rok urodzenia osoby
 * - Łańcuch "kobieta" lub "mężczyzna", w zależności od płci
 *
 */

public class PESEL {

	
	//static ...
	
	
	
	
	public static void main(String[] args) {
		String[] PESELNumbers = new String[] {
				"60081702293", //mężczyzna, data 17.08.1960
				"50032018274", //mężczyzna, data 20.03.1950
				"50072815608", //kobieta, rok 28.07.1950
				"70092408808", //kobieta, rok 24.09.1970
				"71092408808", //nieprawidłowy
				"51032018274", //nieprawidłowy
				"12300200150", //mężczyzna, data 02.10.2012
		};
		
		String[] result = new String[]{"p", "01.01.1920", "mę"};
		
		for (int i = 0; i < PESELNumbers.length; i++) {
			// String[] result = extract(PESELNumbers[i]);
			if(result[0] == "p") {
				System.out.println(String.format("Pesel %s należy do %s urodzonej w roku %s, której płeć to %s.", result[1], result[2]));
			} else {
				System.out.println("Pesel jest nieprawidłowy");
			}
		}
		
	}
	
}
