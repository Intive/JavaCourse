package pl.intive.javacourse.exercises;


/**
 * Napisz program rysujący trójkąt o zadanej długości.<br/>
 * - na standardowe wejście przyjmij liczbę naturalną n.<br/>
 * - na standardowym wyjściu wypisz gwiadki w liczbie * w pierszym wierszu n *,
 * w drugim rzędzie n-1 * ... * w ostatnim rzędzie 1.<br/>
 * 
 * Na przykład dla n = 3 na wyjściu powinny być narysowany nastepujące znaki:<br/>
 * 
 * ***<br/>
 * **<br/>
 * *<br/>
 *
 */
public class Triangle {

	public static void main(String[] args) {
		
	}
}
