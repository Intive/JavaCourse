package pl.intive.javacourse.exercises;

import java.util.Arrays;
import java.util.Random;

/**
 * Napisz program, ktĂłry utworzy tablicÄ™ 10 liczb caĹ‚kowitych i wypeĹ‚ni jÄ…
 * wartoĹ›ciami losowymi z przedziaĹ‚u od â�’10 do 10. NastpÄ™pnie: - wypisz na
 * ekranie zawartoĹ›Ä‡ tablicy, - wyznacz najmniejszy oraz najwiÄ™szy element w
 * tablicy, - wyznacz Ĺ›redniÄ… arytmetycznÄ… elementĂłw tablicy, - wyznacz ile
 * elementĂłw jest mniejszych, ile wiÄ™kszych od Ĺ›redniej. - wypisz na ekranie
 * zawartoĹ›Ä‡ tablicy w odwrotnej kolejnoĹ›ci, tj. od ostatniego do pierwszego.
 *
 *
 */
public class RandomArray {
	public static void main(String[] args) {
		
		Random rand = new Random();
		
		int[] ints = new int[10];
		int suma = 0;
		int min = 500;
		int max = -500;
		int lowerCount = 0;
		int biggerCount = 0;
		int srednia = 0;
		int[] intsCopy = new int[ints.length];
		
		for (int i = 0; i < ints.length; i++) {
			int r = rand.nextInt(20)-10;
			ints[i] = r;
		}
		
		System.out.println("Tablica losowych elementow:\n" + Arrays.toString(ints) + "\n");
		
		for (int i = 0; i < ints.length; i++) {
			suma = ints[i] + suma;
		}
		srednia = suma/ints.length;
		System.out.println("Średnia wynosi: " + srednia + "\n");
		
		for (int i = 0; i < ints.length; i++) {
			if(ints[i] > srednia)
				biggerCount++;
			else if (ints[i] < srednia)
				lowerCount++;
			else{}
		}
		
		System.out.println("Wieksze od sredniej: " + biggerCount + "\nMniejsze od sredniej: " + lowerCount + "\n");
		
		for (int i = 0; i < ints.length; i++) {
			if (ints[i] < min) 				
				min = ints[i] ;
			if (ints[i] > max)
				max = ints[i];
		}
		
		System.out.println("Minimalny element tablicy: " + min + "\nMaksymalny element tablicy: " + max + "\n");

		int j=0;
		for (int i = ints.length-1; i >= 0; i--) {
			intsCopy[j] = ints[i];
			j++;
		}
		
		System.out.println("Odwrócona tablica: \n" + Arrays.toString(intsCopy));		
	}
}
