package pl.intive.javacourse.exercises;

import java.util.Arrays;

/**
 * Zadanie polega na implementacji metod w których jest komentarz (task) TODO, zgodnie z ich opisem. Zadania dotyczą różnych problemów programistycznych.<br/>
 * <br/>
 * Wszystkie znaki będą drukowalnymi znakami ASCII (z przdziału 32 - 126), czyli bez polskich znaków diakrytycznych.<br/>
 * <br/>
 * Przydatne metody:<br/>
 * <code>
 * String s = ...;<br/>
 * String s1 = s.toLowerCase(); // zwraca nowy napis w którym wszystkie duże litery są zamieniane na małe.<br/>  
 * String s2 = s.toUpperCase(); // zwraca nowy napis w którym wszystkie duże litery są zamieniane na duże.<br/>
 * int i1 = s.length; // zwraca długość napisu.<br/>
 * char[] a1 = s.toCharArray(); // konwertuje napis na tablice znaków.<br/>
 * char ch = ...;<br/>
 * Character.isLetter(ch); // Sprawdza czy znak jest literą.<br/>  
 * </code>
 */
public class Combo {

    /**
     * Metoda zwraca objętość prostopodłościonu o podstawie prostokątu z bokami o długości a, b i wysokości h. Powierzchnia jest liczona wg wzrou: a x b x h.
     * <br/>
     * Założenie: a > 0, b > 0, h > 0.
     * 
     * @param a Długość boku podstawy prostokąta.
     * @param b Długość boku podstawy prostokąta.
     * @param h Wysokość prostopadłościanu.
     * @return Objętość prostopadłościanu.
     */
    static int calculateVolume(int a, int b, int h) {
        // TODO
        return 0;
    }

    static void checkCalculateVolume(int a, int b, int h, int expVolume) {
        testNum++;
        int calcVolume = calculateVolume(a, b, h);
        if (calcVolume == expVolume) {
            validTestNum++;
            System.out.printf("Poprawnie wyliczona objętość, która dla a = %d, b = %d, h = %d wynosi %d\n", a, b, h, calcVolume);
        } else {
            System.out.printf("Niepoprawnie wyliczona objętość dla a = %d, b = %d, h = %d, wyliczona objętość = %d, poprawna objętość = %d.\n", a, b, h,
                    calcVolume, expVolume);
        }
    }

    /**
     * Metoda sprawdza czy współrzędne mieszczą się na ekrenia VGA, czyli czy 1 <= x <= 640 i 1 <= y <= 480. Jeśli tak zwraca true, w przeciwnym przypadku
     * false.
     * 
     * @param x Współrzędna x.
     * @param y Współrzędna y.
     * @return Czy punkt (x, y) leży na ekrenie.
     */
    static boolean validateCoords(int x, int y) {
        // TODO
        return false;
    }

    static void checkValidateCoords(int x, int y, boolean expValue) {
        testNum++;
        boolean calcValue = validateCoords(x, y);
        if (calcValue == expValue) {
            validTestNum++;
            System.out.printf("Poprawnie przeprowadzona walidacja punktów, która dla x = %d, y = %d, zwraca = %b.\n", x, y, calcValue);
        } else {
            System.out.printf("Niepoprawnie przeprowadzona walidacja punktów dla x = %d, y = %d, wynosi = %b, poprawna objętość = %b.\n", x, y, calcValue,
                    expValue);
        }
    }

    /**
     * Metoda zwraca wynik działania. Działanie wykonywane jest na opernadach (argumentach) a i b. Wynik jest zależny od znaku sign.
     * <ul>
     * <li>Dla + - wynikiem jest a + b.</li>
     * <li>Dla - - wynikiem jest a - b.</li>
     * <li>Dla * - wynikiem jest a * b.</li>
     * <li>Dla / - wynikiem jest a / b (dzielenie całkowite). Założenie: b jest różne od 0.</li>
     * <li>Dla ^ - wynikiem jest a do potęgi b. Gdzie b >= 1.</li>
     * </ul>
     * 
     * @param a Pierwszy operand.
     * @param b Drugi operand.
     * @param sign Znak operacji, jeden z: + (dodawanie), - (odejmowanie), * (mnożenie), / (dzielenie), ^ (potęgowanie)
     * @return
     */
    static int calculateValue(int a, int b, char sign) {
        // TODO
        return 0;
    }

    static void checkCalculateValue(int a, int b, char sign, int expValue) {
        testNum++;
        int calcValue = calculateValue(a, b, sign);
        if (calcValue == expValue) {
            validTestNum++;
            System.out.printf("Poprawnie wyliczony wynik dla dla %d %c %d = %d.\n", a, sign, b, calcValue);
        } else {
            System.out.printf("Niepoprawnie wyliczony wynik dla dla %d %c %d = %d, zwrócony wynik = %d.\n", a, sign, b, expValue, calcValue);
        }
    }

    /**
     * Metoda oblicza długość wszystkich 3 napisów - s1, s2, s3. W przypadku, gdy jakiś napis jest
     * <code>null</null> przyjmuje dla niego długość 0. 
     * 
     * @param s1 Pierwszy napis. Może być <code>null</code>.
     * @param s2 Drugi napis. Może być <code>null</code>.
     * @param s3 Trzeci napis. Może być <code>null</code>.
     * @return Sumę długości s1, s2 i s3.
     */
    static int countLength(String s1, String s2, String s3) {
        // TODO
        return 0;
    }
    
    static void checkCountLength(String s1, String s2, String s3, int expValue) {
        testNum++;
        try {
            int calcValue = countLength(s1, s2, s3);
            if (calcValue == expValue) {
                validTestNum++;
                System.out.printf("Poprawnie wyliczona długość dla \"%s\", \"%s\", \"%s\", wynosi %d.\n", s1, s2, s3, calcValue);
            } else {
                System.out.printf("Niepoprawnie wyliczona długość dla \"%s\", \"%s\", \"%s\", wynosi %d, zwrócony wynik = %d.\n", s1, s2, s3, expValue, calcValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda zapisuje dane osobowe do obiektu typu <code>String</code> w formacie:
     * <ul>
     * <li>number - na pole przeznaczone są dokładnie 4 znaki. Jeśli numer nie jest czterocyfrowy uzupełniany jest zerami.</li>
     * <li>firstName - na pole przeznaczone jest dokładnie 20 znaków. Wyrównane do lewej.</li>
     * <li>lastName - na pole przeznaczone jest dokładnie 30 znaków. Wyrównane do lewej.</li>
     * <li>age - na pole przeznaczone są dokładnie 3 znaki. Wyrównane do prawej.</li>
     * <li>avgMonthlySalary - na pole przeznaczone jest dokładnie 9 znaków. Każa liczba powinna mieć przynajmniej jedną cyfrę w części całkowitej i dokładnie 2
     * cyfry w części dzisiętnej. Wyrównane do prawej.</li>
     * <li>sex - na pole przeznaczony jest dokładnie 1 znak.</li>
     * </ul>
     * Wszystkie części powinny być oddzielone dokładnie jedną spacją. Dla przykłady jeśli: number = 12, firstName = "Tomek", lastName = "Szewczyk", wiek = 20,
     * avgMonthlySalary = 3100, sex = 'M', to metoda zwróci:<br/>
     * <tt>0012_Tomek______________Szewczyk__________________20_____3100.00_M</tt><br/>
     * natomiast dla number = 333, firstName = "Anna", lastName = "Kowalska-Radziszewska", wiek = 70, avgMonthlySalary = 0, sex = 'K', to metoda zwróci:<br/>
     * <tt>0999_Anna_______________Kowalska-Radziszewska_____70________0.00_K</tt><br/>
     * 
     * @param number Numer osoby.
     * @param firstName Imię.
     * @param lastName Nazwisko.
     * @param age Wiek.
     * @param avgMonthlySalary Przeciętne miesięczne wynagrodzenie.
     * @param sex Płęć - K - kobieta, M - mężczyzna.
     * @return Sformatowany obiekt typu <code>String</code>.
     */
    static String formatPersonalData(int number, String firstName, String lastName, int age, double avgMonthlySalary, char sex) {
        // TODO
        // Do implementacji metody można użyć String.format -
        // https://docs.oracle.com/javase/8/docs/api/java/lang/String.html#format-java.lang.String-java.lang.Object...-
        return "";
    }

    static void checkFormatPersonalData(int number, String firstName, String lastName, int age, double avgMonthlySalary, char sex, String expValue) {
        testNum++;
        String calcValue = formatPersonalData(number, firstName, lastName, age, avgMonthlySalary, sex);
        if (expValue.equals(calcValue)) {
            validTestNum++;
            System.out.printf("Poprawnie sformatowany napis dla number = %d, firstName = %s, lastName = %s, age = %d, avgMonthlySalary = %f, sex = %c to %s.\n",
                    number, firstName, lastName, age, avgMonthlySalary, sex, calcValue);
        } else {
            System.out.printf(
                    "Niepoprawnie sformatowany napis dla number = %d, firstName = %s, lastName = %s, age = %d, avgMonthlySalary = %f, sex = %c, to %s, zwrócony wynik = %s.\n",
                    number, firstName, lastName, age, avgMonthlySalary, sex, expValue, calcValue);
        }
    }

    /**
     * Metoda zwraca liczbę samogłosek w tekście s.
     * 
     * @param s Tekst. Może być <code>null</code>.
     * @return Liczba samogłosek w tekście.
     */
    static int countVowels(String s) {
        // TODO
        return 0;
    }

    static void checkCountVowels(String s, int expValue) {
        testNum++;
        try {
            int calcValue = countVowels(s);
            if (expValue == calcValue) {
                validTestNum++;
                System.out.printf("Poprawnie wyliczona liczba samogłosek dla s = %s wynosi %d.\n", s, calcValue);
            } else {
                System.out.printf("Niepoprawnie wyliczona liczba samogłosek dla s = %s, wynosi %d, zwrócony wynik = %d.\n", s, expValue, calcValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda zwraca długość najdłuższego słowa w tekście s. Innymi słowy zwraca długość n najdłuższego podciągu a1,a2,...an, w tekście s s1,s2,...sm, takiego,
     * że ai jest literą, n <= m, a1,a2,...an są następującymi bespośrednio po sobie znakami w tekście s.
     * 
     * @param s Tekst. Może być <code>null</code>
     * @return Długość najdłuższego słowa. W przypadku, gdy nie ma żadnej litery lub tekst jest pusty zwraca 0.
     */
    static int findLongestWord(String s) {
        // TODO
        return 0;
    }

    static void checkFindLongestWord(String s, int expValue) {
        testNum++;
        try {
            int calcValue = findLongestWord(s);
            if (expValue == calcValue) {
                validTestNum++;
                System.out.printf("Poprawnie wyliczona długość najdłuższego słowa dla s = %s wynosi %d.\n", s, calcValue);
            } else {
                System.out.printf("Niepoprawnie wyliczona długość najdłuższego słowa dla s = %s, wynosi %d, zwrócony wynik = %d.\n", s, expValue, calcValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Metoda mnoży tablicę a przez mnożnik multiplier. W wyniki zwraca nową tablicę gdzie każdy i element ma wartość równą ai * multiplier.
     * 
     * @param a Tablica.
     * @param multiplier Mnożnik
     * @return Nowa tablica powstała w wyniku mnożenia tablica a i mnożnika multiplier.
     */
    static int[] multiply(int[] a, int multiplier) {
        // TODO
        return null;
    }
    
    static void checkMultiply(int[] a, int multiplier, int[] expValue) {
        testNum++;
        try {
            int[] calcValue = multiply(a, multiplier);
            if (Arrays.equals(expValue, calcValue) && expValue != calcValue) {
                validTestNum++;
                System.out.printf("Poprawnie wyliczona operacja mnożenia tablicy a %s i mnożnika %d wynosi %s.\n", Arrays.toString(a), multiplier, Arrays.toString(calcValue));
            } else if (expValue == calcValue) {
                System.out.printf("Niepoprawne zaimplementowana metoda multiply. Powinna tworzyć nową tablicę.\n");
            } else {
                System.out.printf("Niepoprawnie wyliczona operacja mnożenia tablicy a %s i mnożnika %d wynosi %s, zwrócony wynik = %s.\n", Arrays.toString(a), multiplier, Arrays.toString(expValue), Arrays.toString(calcValue));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Metoda wykonuje transpozycję (przestawienie) macierzy. Zwraca nową macierz która jest transpozycją macierzy wejściowej matrix. 
     * Oznacza to, że jeżeli jest dana macierz wejściowa matrix[n][m] to w wyniku działania metody zwracana jest nowa macierz result[m][n] taka, 
     * że dla każdego i z przedziału <0, n) oraz j z przedziału <0, m) prawidziwe jest że:<br>
     * <code>matrix[i][j] == result[j][i]</code><br/>
     * Więcej informacji <a href="https://pl.wikipedia.org/wiki/Macierz_transponowana">w artykule na wikipedii</a>.
     * 
     * @param matrix Macierz wejściowa.
     * @return Nowa macierz powstała w wyniku transpozycji macierzy matrix.
     */
    static int[][] transposeMatrix(int[][] matrix) {
        // TODO
        return null;
    }
    
    static void checkTransposeMatrix(int[][] matrix, int[][] expValue) {
        testNum++;
        try {
            int[][] calcValue = transposeMatrix(matrix);
            if (Arrays.deepEquals(expValue, calcValue)) {
                validTestNum++;
                System.out.printf("Poprawnie wyliczona operacja transpozyci dla macierzy %s wynosi %s.\n", Arrays.deepToString(matrix), Arrays.deepToString(calcValue));
            } else {
                System.out.printf("Niepoprawnie wyliczona operacja transpozyci dla macierzy %s wynosi %s, zwrócony wynik = %s.\n", Arrays.deepToString(matrix), Arrays.deepToString(expValue), Arrays.deepToString(calcValue));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // calculateVolume (0.5)
        System.out.println("Testowanie metody calculateVolume");
        // prostopadłościan
        checkCalculateVolume(2, 2, 2, 8);
        // różne boki
        checkCalculateVolume(2, 3, 4, 24);
        // inny przykład
        checkCalculateVolume(10, 2, 5, 100);
        System.out.println();

        // validateCoords (1)
        System.out.println("Testowanie metody validateCoords");
        // jest w środku
        checkValidateCoords(10, 20, true);
        // jest w środku
        checkValidateCoords(300, 400, true);
        // jest w środku, sprawdzanie czy nie zamienion x i y
        checkValidateCoords(500, 300, true);
        // sprawdzanie czy nie zamienion x i y
        checkValidateCoords(300, 500, false);
        // krawędź lewa
        checkValidateCoords(1, 100, true);
        // krawędź dolna
        checkValidateCoords(100, 1, true);
        // krawędź prawa
        checkValidateCoords(640, 100, true);
        // krawędź górna
        checkValidateCoords(100, 480, true);
        // róg
        checkValidateCoords(640, 480, true);
        // nad
        checkValidateCoords(200, 600, false);
        // pod
        checkValidateCoords(200, -100, false);
        // po lewej
        checkValidateCoords(-200, 400, false);
        // po prawej
        checkValidateCoords(800, 400, false);
        // poza
        checkValidateCoords(-10, -10, false);
        // poza
        checkValidateCoords(1000, 1000, false);
        //
        checkValidateCoords(0, 100, false);
        //
        checkValidateCoords(100, 0, false);
        System.out.println();

        // calculateValue (5)
        System.out.println("Testowanie metody calculateValue");
        // dodawanie
        checkCalculateValue(1, 2, '+', 3);
        checkCalculateValue(20, 15, '+', 35);
        // odejmowanie
        checkCalculateValue(20, 15, '-', 5);
        checkCalculateValue(5, 15, '-', -10);
        checkCalculateValue(10, 10, '-', 0);
        // mnożenie
        checkCalculateValue(20, 5, '*', 100);
        checkCalculateValue(4, 8, '*', 32);
        checkCalculateValue(0, 20, '*', 0);
        checkCalculateValue(-5, 1, '*', -5);
        // dzielenie
        checkCalculateValue(20, 5, '/', 4);
        checkCalculateValue(5, 5, '/', 1);
        checkCalculateValue(-49, 7, '/', -7);
        checkCalculateValue(49, -7, '/', -7);
        // potęgowanie
        checkCalculateValue(0, 3, '^', 0);
        checkCalculateValue(1, 7, '^', 1);
        checkCalculateValue(2, 4, '^', 16);
        checkCalculateValue(7, 1, '^', 7);
        checkCalculateValue(7, 2, '^', 49);
        System.out.println();

        // countLength (2)
        System.out.println("Testowanie metody countLength");
        // zwykłe testy
        checkCountLength("Marek ", "ma ", " DELL-a", 16);
        checkCountLength("Tytus", "Romek", "i A'Tomek", 19);
        checkCountLength("raz (1)", "dwa (2)", "trzy (3)", 22);
        // puste ciągi
        checkCountLength("Jedno zdanie", "", "", 12);
        checkCountLength("", "", "", 0);
        // null'e
        checkCountLength(null, null, null, 0);
        checkCountLength("", "", null, 0);
        checkCountLength(null, "Programowanie komputerów – proces projektowania, tworzenia, testowania i utrzymywania kodu źródłowego programów komputerowych",
                "", 125);
        System.out.println();

        // formatPersonalData (7.5)
        System.out.println("Testowanie metody formatPersonalData");
        checkFormatPersonalData(12, "Tomek", "Szewczyk", 20, 3100d, 'M', "0012 Tomek                Szewczyk                        20   3100,00 M");
        checkFormatPersonalData(333, "Anna", "Kowalska-Radziszewska", 70, 0d, 'K', "0333 Anna                 Kowalska-Radziszewska           70      0,00 K");
        // minimalne
        checkFormatPersonalData(0, "", "", 0, 0d, 'K', "0000                                                       0      0,00 K");
        // maksymalne
        checkFormatPersonalData(9999, "Heeeermeeeeneeegilda", "Kowalska-Radziszewska-Krawczyk", 123, 123456.78d, 'K',
                                                                                   "9999 Heeeermeeeeneeegilda Kowalska-Radziszewska-Krawczyk 123 123456,78 K");
        System.out.println();

        // countVowels (3)
        System.out.println("Testowanie metody countVowels");
        // różne słowa
        checkCountVowels("Tomek ma laptopa.", 6);
        checkCountVowels("Maoam", 3);
        checkCountVowels("Zab zupa zebowa", 6);
        // puste ciągi
        checkCountVowels(null, 0);
        checkCountVowels("", 0);
        checkCountVowels(" ", 0);
        // inne znaki
        checkCountVowels("!@#$%^&*()-_=+12344567890", 0);
        // spółgłoski
        checkCountVowels("BCDFGHJKLMNPQRSTVWXZbcdfghjklmnpqrstwvxz", 0);
        // samogłoski
        checkCountVowels("AEIOUYaeiouy", 12);
        System.out.println();

        // findLongestWord (10)
        System.out.println("Testowanie metody findLongestWord");
        // różne słowa
        checkFindLongestWord("Tomek ma laptopa.", 7);
        checkFindLongestWord("Maoam", 5);
        checkFindLongestWord("Zab zupa zebowa", 6);
        checkFindLongestWord("Zab_zupa_zebowa", 6);
        checkFindLongestWord("a|b|c|d|e|f|g|h|i|j", 1);
        checkFindLongestWord("Ania, Justyna, Magda", 7);
        checkFindLongestWord(" z   abcdefghij aaa  ", 10);
        // puste ciągi
        checkFindLongestWord(null, 0);
        checkFindLongestWord("", 0);
        checkFindLongestWord(" ", 0);
        // inne znaki
        checkFindLongestWord("!@#$%^&*()-_=+12344567890", 0);
        System.out.println();
        
        // multiply (4)
        System.out.println("Testowanie metody multiply");
        // 1 leement
        int[] a1 = {1};
        checkMultiply(a1, 1, a1);
        int[] a2 = {5};
        checkMultiply(a1, 5, a2);
        int[] a3 = {0};
        checkMultiply(a1, 0, a3);
        // wiele elementów
        int[] a4 = {1, 2, 3, 4, 5};
        checkMultiply(a4, 1, a4);
        int[] a5 = {5, 10, 15, 20, 25};
        checkMultiply(a4, 5, a5);
        int[] a6 = {0, 0, 0, 0, 0};
        checkMultiply(a4, 0, a6);
        System.out.println();
        
        // transposeMatrix (8)
        System.out.println("Testowanie metody transposeMatrix");
        // różne
        int[][] m1 = {{1, 2, 3}, {4, 5, 6}};
        int[][] m2 = {{1, 4}, {2, 5}, {3, 6}};
        checkTransposeMatrix(m1, m2);
        int[][] m3 = {{10, 18, 17}, {22, 24, 21}, {35, 31, 37}};
        int[][] m4 = {{10, 22, 35}, {18, 24, 31}, {17, 21, 37}};
        checkTransposeMatrix(m3, m4);
        int[][] m5 = {{51, 52, 53}, {61, 62, 63}, {71, 72, 73}, {81, 82, 83}};
        int[][] m6 = {{51, 61, 71, 81}, {52, 62, 72, 82}, {53, 63, 73, 83}};
        checkTransposeMatrix(m5, m6);
        checkTransposeMatrix(m6, m5);
        // symetryczna 
        int[][] m7 = {{100, 100, 100}, {100, 100, 100}, {100, 100, 100}};
        checkTransposeMatrix(m7, m7);
        int[][] m8 = {{1000}};
        checkTransposeMatrix(m8, m8);
        // 1 wiersz 
        int[][] m9 = {{13, 22, 81, 74, 39}};
        int[][] m10 = {{13}, {22}, {81}, {74}, {39}};
        checkTransposeMatrix(m9, m10);
        // 1 kolumna 
        checkTransposeMatrix(m10, m9);
        
        
        System.out.printf("Wykonano poprawnie %d testów z %d", validTestNum, testNum);
    }

    private static int testNum = 0;
    private static int validTestNum = 0;
}