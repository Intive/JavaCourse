package pl.intive.javacourse.exercises;

public class MacierzePierwszaMetoda {

	public static void dodajMacierz(int[][] wynik, int[][] macierz1, int[][] macierz2) {
		for (int i = 0; i < wynik.length; i++) {
			for (int j = 0; j < wynik[i].length; j++) {
				wynik[i][j] = macierz1[i][j] + macierz2[i][j];
			}
		}
	}

	public static void wyswMacierz(int[][] m) {
		for (int i = 0; i < m.length; i++) {
			int[] wiersz = m[i];
			for (int j = 0; j < wiersz.length; j++) {
				System.out.print(wiersz[j]);
				if (j != wiersz.length - 1) {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		System.out.println();

	}

	public static void main(String[] args) {

		int[][] macierz = new int[][] { new int[] { 1, 3, 5 }, new int[] { 1, 3, 5 }, new int[] { 1, 7, 5 } };
		int[][] macierz2 = new int[][] { new int[] { 2, 1, 2 }, new int[] { 21, 23, 25 }, new int[] { 21, 27, 25 } };
		int[][] macierz3 = new int[][] { new int[] { 2, 1, 2 }, new int[] { 21, 23, 25 }, new int[] { 21, 27, 25 } };

		wyswMacierz(macierz);
		wyswMacierz(macierz2);
		wyswMacierz(macierz3);

		int[][] wynik = new int[3][3];
		
		dodajMacierz(wynik,  macierz, macierz2);

//		// dodawanie
//		for (int i = 0; i < wynik.length; i++) {
//			for (int j = 0; j < wynik[i].length; j++) {
//				wynik[i][j] = macierz[i][j] + macierz2[i][j];
//
//			}
//		}
		
		dodajMacierz(wynik, wynik, macierz3);
//		for (int i = 0; i < wynik.length; i++) {
//			for (int j = 0; j < wynik[i].length; j++) {
//				wynik[i][j] = wynik[i][j] + macierz3[i][j];
//			}
//		}

		wyswMacierz(wynik);
	}

}
