package pl.intive.javacourse.exercises;

import java.util.Scanner;

/**
 * Napisz program, który przyjmuje na wejściu rok i miesiąc (jako numer od 1 dla stycznia do 12 dla grudnia), 
 * a następnie wypisuje na wyjściu liczbę dni w danym miesiącu. W przypadku, gdy użytkownik podał miesiąc 
 * spoza zakresu 1..12 należy wypisać na wyjście komunikat z informacją o błędzie. 
 * Proszę uwzględnić rok przestępny. Latami przestępnymi są te, których numeracja:
 * - jest podzielna przez 4 i niepodzielna przez 100, lub
 * - jest podzielna przez 400.
 */
public class DaysInMonth {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Proszę podać rok: ");
		int year = scanner.nextInt();
		System.out.println("Proszę podać miesiąc (1-12): ");
		int month = scanner.nextInt();
		scanner.close();
		
		// ...
		
		// Wyliczona liczba dni
		System.out.println("1");
	}
}