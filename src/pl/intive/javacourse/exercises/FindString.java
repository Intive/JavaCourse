package pl.intive.javacourse.exercises;


/**
 * Napisz program składający się z trzech metod:
 * <ul>
 * <li><code>findString</code> - która przyjmuje na wejściu napis s typu String i wzorzec p typu String 
 * i zwraca wartośc true, jeśli możliwe jest takie usunięcie liter z napisu s by był identyczny jak wzorzec p 
 * - znajduje wzorzec p w napisie s. Nie można zmieniać kolejności liter. <br/>
 * Np.: dla s = JJaavvaa i p - Java powinien zwrócić true - gdyż po usunięciu co drugiej litery z s zostanie p.
 * dla s = Jaca a p = Java powinien zwrócić false.</li>
 * <li><code>check</code> - która przyjmuje napis s typu String, wzorzec p typu String i zmienną r typu boolean. 
 * Metoda powinna wywoływać metodę findString z parametrami s i p, oraz sprawdzać czy zwrócony przez metodę 
 * findString wynik jest taki sam jak wartość r przekazana do metody. Następnie powinna wyświetlać informacje o resultacie wywołania.
 * Np.: "Metodę findString dla napisu = "JJaavvaa" i wzorca = "Java" zwróciła poprawny wynik "true". </li>
 * <li><code>main</code> - która urachamia program i zawiera wywołania metody check, które umożliwiają przetestowanie metody findString.<br/>
 * Np.:<br/>
 * <code> 
 * check("JJaavvaa", "Java", true); <br/>
 * check("Jaca", "Java", false); 
 * </li>
 */
public class FindString {
	
	public static boolean findString(String s, String p) {
		char[] wzor = p.toCharArray();
		char[] napis = s.toCharArray();
		return false;
	}
	public static void check(String s, String p, boolean res){
		boolean result = findString(s, p);
		
	}
	public static void main(String[] args){
		
	}
}
