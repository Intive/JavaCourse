package pl.intive.javacourse.exercises;

/**
 * Sprawdź czy punkt p(xp, yp) należy do prostokąta (x1,y1), (x1,y2), (x2,y2), (x2, y1).  
 */
public class PointInRectagle {

	public static void main(String[] args) {
		int x1 = 1, x2 = 9, y1 = 1, y2 = 5;
		int xp = 2, yp = 5; // jest
		//xp = 1; // krawędź
		//xp = 0; // nie jest
		//xp = 2; yp = 5; // krawędź
		//yp = 10; // nie jest
		
		// Czy punkt należy do prostokąta
		System.out.println(true);
	}
}