package pl.intive.javacourse.exercises;


/**
 * Napisz program wyświetlający macierz oraz wykonujący następujące obliczenia dla macierzy:<br/>
 * <ul>
 * <li>sumę,</li>
 * <li>różnicę,</li>
 * <li>wyznacznik,</li>
 * <li>iloczyn.</li>
 * </ul>
 * 
 * Poszczególne działania powinny być wykonane w osobnych metodach.<br/>
 * 
 * By wartości z tej samej kolumny wyświetlały się jedna pod drugą, należy użyć metody
 * <code>System.out.printf("%5d", value)</code> zamiast <code>System.out.print(value)</code>,
 * gdzie value oznacza wartość do wyświetlania.
 * 
 * @author krzysztof.atlasik
 */
public class Matrix {

	/**
	 * Zwraca nową macierz, która jest wynikiem działania: a + b, gdzie a i b to macierze.<br/>
	 * Macierze muszą mieć takie same wymiary by można było je dodać.
	 * 
	 * @param a Macierz a.
	 * @param b Macierz b.
	 * @return Wynik dodawania macierzy.
	 */
	static int[][] addMatrix(int[][] a, int[][] b) {
		// TODO
		return null;
	}
	
	/**
	 * Wyświetla macierz a na standardowe wyjście.
	 * 
	 * @param a Macierz
	 */
	static void printMatrix(int[][] a) {
		// TODO
	}
	
	public static void main(String[] args) {
		
		int[][] a = new int[][]{
			{1, 2, 3},
			{1, 4, 6},
			{3, 4, 3}
		};
		
		int[][] b = new int[][]{
			{6, 2, 3},
			{5, 1, 6},
			{4, 4, 3}
		};

		int[][] c = new int[][]{
			{1, 2},
			{3, 4},
			{5, 6}
		};

		int[][] d = new int[][]{
			{10, 9, 8},
			{7, 6, 5},
		};
		
		
		System.out.println("Macierz a:");
		printMatrix(a);
		System.out.println();
		System.out.println("Macierz b:");
		printMatrix(b);
		System.out.println();
		System.out.println("Macierz c:");
		printMatrix(c);
		System.out.println();
		System.out.println("Macierz d:");
		printMatrix(d);
		System.out.println();
		
		System.out.println("a + b = ");
		printMatrix(addMatrix(a, b));
		System.out.println();
	}
}