package pl.intive.javacourse.exercises;

import java.util.Arrays;

/**
 * Napisz program, który tworzy i wypełnia tablicę z liczbami. Na wejściu zadany jest maksymalna wartość max i interwał i. 
 * Tablica wypełniana jest liczbami 0, i, 2*i itp dopóki prawdziwe jest, że k * i <= max.<br/>
 * Np.: dla max = 10 oraz i = 2 w tablicy powinny być elementy {0, 2, 4, 6, 8, 10}.
 */
public class InterNumbers {

    /**
     * Na wejściu zadany jest maksymalna wartość max i interwał i. Tablica wypełniana jest liczbami 0, i, 2*i itp.
     * dopóki prawdziwe jest, że k * i <= max.<br/>
     * Np.: dla max = 10 oraz i = 2 w tablicy powinny być elementy {0, 2, 4, 6, 8, 10}.<br/>
     * Jeżeli interwał jest mniejszy lub równy 0 lub maksymalna wartość jest mniejsza od 0 należy zwrócić pustą tablicę.
     * 
     * @param max Maksymalna wartość
     * @param interval Interwał
     * @return Tablica wypełniona wartościami.
     */
    static int[] interNumbers(int max, int interval) {
        return null;
    }

    /**
     * Metoda, która wywołuje metodę wypełniającą tablicę, sprawdza czy wynik jest poprawny,
     * porównując go z tablicą expRes, a następnie wyświetla informacje o poprawności wyniku. 
     * 
     * @param max Maksymalna wartość
     * @param interval Interwał
     * @param expRes Oczekiwana tablica z wynikiem.
     */
    static void check(int max, int interval, int[] expRes) {
        int[] res = interNumbers(max, interval);
        boolean ok = true;
        if (res == null || res.length != expRes.length) {
            ok = false;
        } else {
            for (int i = 0; i < expRes.length; i++) {
                if (res[i] != expRes[i]) {
                    ok = false;
                }
            }
        }

        if (ok) {
            System.out.println("Tablica została wypełniona poprawnie:");
            System.out.println(Arrays.toString(res));
        } else {
            System.out.println("Tablica została wypełniona niepoprawnie. Zwrócona tablica:");
            System.out.println(Arrays.toString(res));
            System.out.println("Oczekiwana tablica:");
            System.out.println(Arrays.toString(expRes));
        }

        System.out.println();
    }

    public static void main(String[] args) {

        // Wywołanie kolejnych testów metody sumującej.

        // Jednen element.
        int[] resTiny = { 0, 1 };
        check(1, 1, resTiny);

        // Prosty przykład
        int[] res1 = { 0, 2, 4, 6, 8, 10 };
        check(10, 2, res1);

        // Różne wartości
        int[] res2 = { 0, 3, 6, 9, 12 };
        check(14, 3, res2);

        // Interwał większy od maksymalnej wartości
        int[] res3 = { 0 };
        check(5, 10, res3);

        // Maksymalna wartość = 0
        int[] resZero = { 0 };
        check(0, 10, resZero);

        // duże wartości
        int[] resBigVal = { 0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };
        check(1000, 100, resBigVal);

        int[] resNegMax = { };
        check(-1, 1, resNegMax);

        int[] resNegInter = { };
        check(5, -1, resNegInter);
    }
}
