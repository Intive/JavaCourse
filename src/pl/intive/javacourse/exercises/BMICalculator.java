package pl.intive.javacourse.exercises;

import java.util.Scanner;

/**
 * Napisz program obliczający czy użytkownik ma prawidłową wagę.
 * 
 * Na standardowe wejście przyjmij następujące dane - imię - wagę w kg - wzrost
 * w m
 * 
 * Nąstępnie oblicz BMI używając wzoru waga/(wzrost w m)^2
 * 
 */

public class BMICalculator {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Podaj swoje imię szczęściarzu:");
		String imię = scanner.nextLine();
		System.out.println("Podaj swoją wagę w kg... jeśli się odważysz:");
		double waga = scanner.nextDouble();
		System.out.println("Podaj swój wzrost w m:");
		double wzrost = scanner.nextDouble();
		
		double BMI = waga/(wzrost*wzrost);
		System.out.printf("Twój wskażnik BMI to - %.2f\n" , BMI);

		if (BMI > 30) {
			System.out.println("Spasłeś/aś się!");
		} else if (BMI > 25) {
			System.out.println("Poćwicz.");
		} else if (BMI > 20) {
			System.out.println("Tak Trzymaj!!");
		} else {
			System.out.println("Idż coś zjedz!!!");
		}

	}

}
