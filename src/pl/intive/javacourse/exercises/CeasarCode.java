package pl.intive.javacourse.exercises;

import java.util.Scanner;

/**
 * Napisz program obliczający kod cezara z łacucha znakowego podanego na wejście
 *
 * Na standardowe wejście przyjmowany jest łancuch znakowy, potem wszystkie duże
 * litery zmieniane są na małe, następnie wszystkie litery przesuwane są o 13
 * miejsc w alfabecie i wypisywane na ekran.
 */
public class CeasarCode {
    
    public static void main(String[] args) {                                //<-- dodajemy klasę
        Scanner scanner = new Scanner(System.in);                           //<-- dodajemy skaner
        System.out.println("Podaj wiadomość:");                             //<-- Wyświetlamy co wpisać
        String wiadomosc = scanner.nextLine();                              //<-- Tworzy wiadomość

        char[] Tablicazwiadomoscia = wiadomosc.toCharArray();               //<-- konwertuje do ASCII
        for (int index = 0; index <= Tablicazwiadomoscia.length - 1; index++) {  //<-- tworzymu pętlę (1 - określamy index 2 - tworzymy warunek 3 - zwiększamy index)
            char szyfr = (char) (Tablicazwiadomoscia[index] + 13);              //<-- zamieniamy int na char (rzutujemy)
            Tablicazwiadomoscia[index] = szyfr;                             //<-- zaszyfrowany tekst tablicy
        }
        String cezar = new String(Tablicazwiadomoscia);                     //<-- zamieniamy tablicę na String
        System.out.println(cezar);                                          //<-- wyświetlamy zaszyfrowany tekst

        for (int index = 0; index <= Tablicazwiadomoscia.length - 1; index++) { //<-- odszyfrowanie wiadomości
            char szyfr = (char) (Tablicazwiadomoscia[index] - 13);
            Tablicazwiadomoscia[index] = szyfr;
        }
        String odkodowane = new String(Tablicazwiadomoscia);                    //<-- wyświetlamy odszyfrowaną wiadomość
        System.out.println(odkodowane);
    }
}
