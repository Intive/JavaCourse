package pl.intive.javacourse.exercises;

import java.util.Arrays;

public class SitoKogostam {
	public static void main(String[] args) {
		int[] tableNumbers = makeTable(15);
		System.out.println(Arrays.toString(tableNumbers));
		System.out.println(Arrays.toString(findPrimes(tableNumbers)));
		System.out.print("[");
		for (int index = 0; index < tableNumbers.length; index++) {
			if (tableNumbers[index] > 0) {
				System.out.print(tableNumbers[index]+ ", ");
			}
		}
		System.out.print("]");
	}

	static int[] makeTable(int zakres) {
		int[] basicTable = new int[zakres - 1];
		for (int x = 0; x < basicTable.length; x++) {
			basicTable[x] = 2 + x;
		}
		return basicTable;
	}

	static int[] findPrimes(int[] basicTable) {
		for (int y = 2; y <= basicTable.length; y++) {

			int mnożnik = 2;
			int liczbaDoWykreslenia = mnożnik++ * y;
			while ((liczbaDoWykreslenia - 2) < basicTable.length) {
				basicTable[liczbaDoWykreslenia - 2] = 0;
				liczbaDoWykreslenia = y * mnożnik++;

			}
		}
		return basicTable;

	}
}
