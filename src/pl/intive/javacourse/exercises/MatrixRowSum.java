package pl.intive.javacourse.exercises;

/**
 * Napisz program, który oblicza sumę pojedyńczej kolumny w macierzy oraz wyświetla wynik obliczeń: w pierwszych wierszach macierz, a w ostatnim sumę. 
 * Ostatni wiersz jest oddzielony od poprzednich wierszem ze znakami "=".<br/>
 * Założenie: wszystkie wartości, zarówno w macierzy wejściowej jak i tablicy z wynikami są z przedziału 0..9999.<br/>
 * 
 * Należy napisać tylko kod w miejscach oznaczonych jako TODO.<br/>
 * 
 * By wartości z tej samej kolumny wyświetlały się jedna pod drugą, należy użyć metody
 * <code>System.out.printf("%5d", value)</code> zamiast <code>System.out.print(value)</code>,
 * gdzie value oznacza wartość do wyświetlania.<br/>
 */
public class MatrixRowSum {

	
	/**
	 * Metoda oblicza sumę wartości elementów w kolumnie i zwraca tablicę z sumami dla poszczególnych kolumn.
	 * Suma kolumny i to matrix[i][0] + matrix[i][1] + ... matrix[i][n-1], 
	 * gdzie n to liczba elementów w kolumnie, a i należy do przedziału 0..m-1, gdzie m to liczba kolumn.
	 * 
	 * @param matrix Macierz.
	 * @return Zwraca tablicę z sumą wartości elemenetów w poczególnych kolumnach. 
	 * @return Długość tablicy równa jest liczbie kolumn w macierzy.
	 */
	static int[] sumColumns(int[][] matrix) {
		int[] sumaKolumn = new int[matrix.length];
				for (int kolumna = 0; kolumna < matrix.length; kolumna++) {
					for (int wiersz = 0; wiersz < matrix[kolumna].length; wiersz++) {
						
					}
				}
		return null;
	}
	
	/**
	 * Metoda wyświetlająca macierz i wynik na standardowe wyjście. Każdy wiersz powinien być w jednej linii, a wartości przechowywane w 5 polach.
	 * W ostatniej linii jest wyświetlona suma. Ostatia linia jest oddzielona od poprzednich wierszem ze znakami "=".<br/>
	 * Przykładowa macierz ma postać:<br/>
	 * <code>
	 * &nbsp;&nbsp;&nbsp;&nbsp;1&nbsp;&nbsp;&nbsp;&nbsp;2&nbsp;&nbsp;&nbsp;&nbsp;3<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;&nbsp;6<br/>
	 * ===============<br/>
	 * &nbsp;&nbsp;&nbsp;&nbsp;5&nbsp;&nbsp;&nbsp;&nbsp;7&nbsp;&nbsp;&nbsp;&nbsp;9
	 * </code>
	 * 
	 * @param matrix Macierz.
	 * @param result Tablica z wynikami dodawania elementów z każdej kolumny.
	 */
	static void printMatrixAndRes(int[][] matrix, int[] result) {
		// TODO 
	}
	
	/**
	 * Metoda, która wywołuje metodę wyliczającą sumę elementów w kolumnie, sprawdza czy wynik jest poprawny,
	 * porównując go z tablicą expRes, a następnie wyświetla informacje o poprawności wyniku 
	 * i macierz z wynikiem na standardowym wyjściu.
	 * 
	 * @param matrix Macierz.
	 * @param expRes Oczekiwany wynik dodawania.
	 */
	static void check(int[][] matrix, int[] expRes) {
		int[] res = sumColumns(matrix);
		if (res == null || res.length != expRes.length) {
			System.out.println("Zwrócony wiersz jest niepoprawny.");
			return;
		}
		
		boolean ok = true;
		for (int i = 0; i < expRes.length; i++) {
			if (res[i] != expRes[i]) {
				ok = false;
			}
		}
		
		if (ok) {
			System.out.println("Suma została wyliczona poprawnie:");
			printMatrixAndRes(matrix, res);
		} else {
			System.out.println("Suma została wyliczona niepoprawnie. Zwrócona suma:");
			printMatrixAndRes(matrix, res);
			System.out.println("Oczekiwana suma:");
			printMatrixAndRes(matrix, expRes);
		}
		
		System.out.println();
	}
	
	public static void main(String[] args) {
		
		// Wywołanie kolejnych testów metody sumującej.

		// Najprostsza macierz z jednym elementem.
		int[][] matrixTiny = { {10} };
		int[] resTiny = {10};
		check(matrixTiny, resTiny);

		// Prosta macierz
		int[][] matrix1 = {
				{1, 1, 1},
				{2, 2, 2},
				{3, 3, 3},
		};
		int[] res1 = {3, 6, 9};
		check(matrix1, res1);
		
		// Różne wartości
		int[][] matrix2 = {
				{1, 7, 22, 44, 26},
				{2, 12, 70, 7, 9},
				{90, 2, 3, 3, 2},
				{20, 20, 20, 20, 20},
				{18, 33, 6, 25, 18},
				{1, 4, 16, 64, 15},
				{18, 19, 20, 21, 22},
		};
		int[] res2 = {100, 100, 100, 100, 100, 100, 100};
		check(matrix2, res2);
		
		// Jeden wiersz
		int[][] matrix1Row = {{3}, {6}, {9}, {12}, {15}};
		int[] res1Row = {3, 6, 9, 12, 15};
		check(matrix1Row, res1Row);
		
		// Jedna kolumna
		int[][] matrix1Col = {{3, 6, 9, 12, 15}};
		int[] res1Col = {45};
		check(matrix1Col, res1Col);
		
		// duże wartości
		int[][] matrixBigVal = {
				{10, 950, 2300},
				{3000, 3000, 3000},
				{1, 2, 3},
		};
		int[] resBigVal = {3260, 9000, 6};
		check(matrixBigVal, resBigVal);
	}
}