package pl.intive.javacourse.scanner;

import java.util.Scanner;

public class ScannerExample {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Podaj imię:");
		System.out.println("Witaj " + scanner.nextLine());

		System.out.println("Podaj wiek:");
		System.out.println("Masz " + scanner.nextInt() + " lat");

		System.out.println("Podaj swój wzrost:");
		System.out.println("Masz " + scanner.nextDouble() + " centymetrów wzrostu");

		scanner.close();

	}

}
