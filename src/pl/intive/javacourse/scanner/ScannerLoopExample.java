package pl.intive.javacourse.scanner;

import java.util.Scanner;

public class ScannerLoopExample {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		int sum = 0;

		while (true) {
			System.out.println("Podaj liczbę całkowitą:");

			sum += scanner.nextInt();

			System.out.println("Suma podanych liczb to: " + sum);

			if (sum > 1000) {
				System.out.println("Suma przekroczyła już 1000. Koniec zabawy! Zabieraj się do dalszej nauki!125");
				break;
			}
		}

		System.out.println("Suma końcowa: " + sum);

		scanner.close();

	}

}
