package pl.intive.javacourse.factorial;

import java.math.BigInteger;
import java.util.Scanner;

public class FactorialWithBigInteger {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		System.out.println("Podaj liczbę:");
		int number = scanner.nextInt();
		BigInteger result = BigInteger.valueOf(1);

		for (int i = 2; i < number; i++) {
			result = result.multiply(BigInteger.valueOf(i));
		}

		System.out.println("Silnia z liczby " + number + " wynosi: " + result);

		scanner.close();
	}

}
