package pl.intive.javacourse.oop.interfaces;

import java.util.ArrayList;
import java.util.List;


public class App {

    public static void main(String[] args) {
        List<UIElement> elements = new ArrayList<>();
         
        Window window = new Window("Jakieś okno", 100, 100, 500, 200);
        elements.add(window);
        
        Button buttonGreen = new Button(UIElementSkeleton.COLOR_GREEN, 200, 300);
        elements.add(buttonGreen);
        
        Button buttonRed = new Button(UIElementSkeleton.COLOR_RED, 400, 300);
        elements.add(buttonRed);
        
        for (UIElement element : elements) {
            element.paint();
        }
        
        window.onClick(new ClickEvent(110, 110));
        buttonRed.onClick(new ClickEvent(210, 310));
        buttonGreen.onClick(new ClickEvent(410, 310));
        window.onMove(new MoveEvent(110, 110));
        window.onMove(new MoveEvent(120, 110));
        window.onMove(new MoveEvent(130, 110));
        
        for (UIElement element : elements) {
            element.paint();
        }
    }
}
