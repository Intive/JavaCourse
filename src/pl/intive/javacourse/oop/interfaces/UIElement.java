package pl.intive.javacourse.oop.interfaces;

public interface UIElement {

    String getName();
    
    void paint();
}
