package pl.intive.javacourse.oop.interfaces;

public abstract class Event {

    int x;
    int y;
    
    public Event(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
