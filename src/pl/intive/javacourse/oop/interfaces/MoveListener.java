package pl.intive.javacourse.oop.interfaces;

public interface MoveListener {

    void onMove(MoveEvent moveEvent);
}
