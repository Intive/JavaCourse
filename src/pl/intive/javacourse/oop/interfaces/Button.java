package pl.intive.javacourse.oop.interfaces;

public class Button extends UIElementSkeleton implements ClickListener {

    private int x;
    private int y;
    private final int xSize = 100;
    private final int ySize = 50;
    
    protected Button(String color, int x, int y) {
        super(color);
        this.x = x;
        this.y = y;
    }

    @Override
    public String getName() {
        return "Przycisk";
    }

    @Override
    protected String getDetails() {
        return String.format("x: %d, y: %d, xSize: %d, ySize: %d", x, y, xSize, ySize);
    }

    @Override
    public void onClick(ClickEvent clickEvent) {
        System.out.println("Naciśnięto przycisk");
    }
}
