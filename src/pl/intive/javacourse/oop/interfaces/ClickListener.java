package pl.intive.javacourse.oop.interfaces;

public interface ClickListener {

    void onClick(ClickEvent clickEvent);
}
