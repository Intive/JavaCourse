package pl.intive.javacourse.oop.interfaces;

public class Window extends UIElementSkeleton implements ClickListener, MoveListener {

    private String title;
    private int x;
    private int y;
    private int xSize;
    private int ySize;
    
    public Window(String title, int x, int y, int xSize, int ySize) {
        super(COLOR_WHITE);
        this.title = title;
        this.x = x;
        this.y = y;
        this.xSize = xSize;
        this.ySize = ySize;
    }

    @Override
    public String getName() {
        return "Okno";
    }

    @Override
    protected String getDetails() {
        return String.format("title: %s, x: %d, y: %d, xSize: %d, ySize: %d", title, x, y, xSize, ySize);
    }

    @Override
    public void onClick(ClickEvent clickEvent) {
        if (COLOR_WHITE.equals(color)) {
            color = COLOR_BLACK;
        } else {
            color = COLOR_WHITE;
        }
    }

    @Override
    public void onMove(MoveEvent moveEvent) {
        System.out.printf("Zarejestrowana zdarzanie przesunięcia kursora, współrzędne x: %d, y: %d\n", moveEvent.x, moveEvent.y);
    }
}