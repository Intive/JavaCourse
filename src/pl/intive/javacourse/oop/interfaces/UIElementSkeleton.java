package pl.intive.javacourse.oop.interfaces;

public abstract class UIElementSkeleton implements UIElement {

    public static final String COLOR_WHITE = "Biały";
    public static final String COLOR_RED = "Czerwony";
    public static final String COLOR_GREEN = "Zielony";
    public static final String COLOR_BLACK = "Czarny";
    
    protected String color;

    protected UIElementSkeleton(String color) {
        this.color = color;
    }
    
    @Override
    public void paint() {
        System.out.printf("To jest: %s, color: %s, %s\n", getName(), color, getDetails());
    } 
    
    abstract protected String getDetails(); 
}
