package pl.intive.javacourse.oop.interfaces;

public class MoveEvent extends Event {

    public MoveEvent(int x, int y) {
        super(x, y);
    }
}
