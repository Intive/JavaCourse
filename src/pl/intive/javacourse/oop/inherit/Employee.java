package pl.intive.javacourse.oop.inherit;

public class Employee extends Person {

    private Manager boss;
    private String orgUnit;
    private String position;
    
    public Employee() {
    }

    public Manager getBoss() {
        return boss;
    }

    public void setBoss(Manager boss) {
        this.boss = boss;
    }
    
    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String getShortInfo() {
        String shortInfo = super.getShortInfo();
        if (boss != null) {
            shortInfo += ", szef: (" + boss.getShortInfo() + ")";
        }
        return shortInfo;
    }
}