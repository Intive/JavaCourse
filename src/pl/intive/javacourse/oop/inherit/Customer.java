package pl.intive.javacourse.oop.inherit;

import java.util.ArrayList;
import java.util.List;

public class Customer extends Person {

    private final List<String> purchases = new ArrayList<>();
    private Employee accountManager;
    
    public Customer() {
    }

    public List<String> getPurchases() {
        return purchases;
    }

    public Employee getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(Employee accountManager) {
        this.accountManager = accountManager;
    }

    @Override
    public String getShortInfo() {
        String shortInfo = super.getShortInfo();
        if (purchases.isEmpty()) {
            shortInfo += ", nie dokonywał jeszcze zakupów";
        } else {
            shortInfo += ", dokonał " + purchases.size() + " zakupów";
        }
        if (accountManager != null) {
            shortInfo += ", opiekun klienta: (" + accountManager.getShortInfo() + ")";
        }
        return shortInfo;
    }
}
