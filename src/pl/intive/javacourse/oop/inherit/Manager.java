package pl.intive.javacourse.oop.inherit;

import java.util.ArrayList;
import java.util.List;

public class Manager extends Employee {

    private final List<Employee> subordinates = new ArrayList<>();
    
    public Manager() {
    }

    public List<Employee> getSubordinates() {
        return subordinates;
    }

    @Override
    public String getShortInfo() {
        return super.getShortInfo() + ", liczba podwładnych: " + subordinates.size();
    }
}