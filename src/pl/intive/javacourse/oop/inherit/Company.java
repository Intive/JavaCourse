package pl.intive.javacourse.oop.inherit;

import java.util.ArrayList;
import java.util.List;

public class Company {

    public static void main(String[]args) {
        // Tworzenie modelu 
        int id = 1;
        String orgUnit = "Sprzedaż";
        List<Person> persons = new ArrayList<>();
        
        Employee employee1 = new Employee();
        employee1.setId(id++);
        employee1.setFirstName("Tomasz");
        employee1.setLastName("Kowlaski");
        employee1.setOrgUnit(orgUnit);
        employee1.setPosition("Junior Sales Representative");
        persons.add(employee1);

        Employee employee2 = new Employee();
        employee2.setId(id++);
        employee2.setFirstName("Adam");
        employee2.setLastName("Nowak");
        employee2.setOrgUnit(orgUnit);
        employee2.setPosition("Sales Representative");
        persons.add(employee2);
        
        Employee employee3 = new Employee();
        employee3.setId(id++);
        employee3.setFirstName("Sebastian");
        employee3.setLastName("Woźniak");
        employee3.setOrgUnit("HR");
        employee3.setPosition("HR Specialist");
        persons.add(employee3);

        Manager manager = new Manager();
        manager.setId(id++);
        manager.setFirstName("Marcin");
        manager.setLastName("Zieliński");
        manager.setOrgUnit(orgUnit);
        manager.setPosition("Team Leader");
        List<Employee> subordinates = manager.getSubordinates();
        subordinates.add(employee1);
        employee1.setBoss(manager);
        subordinates.add(employee2);
        employee2.setBoss(manager);
        persons.add(manager);

        Customer customer1 = new Customer();
        customer1.setId(id++);
        customer1.setFirstName("Paweł");
        customer1.setLastName("Kwiatkowski");
        persons.add(customer1);
        
        Customer customer2 = new Customer();
        customer2.setId(id++);
        customer2.setFirstName("Wojciech");
        customer2.setLastName("Mazur");
        customer2.getPurchases().add("Ubezpiecznie 1");
        customer2.getPurchases().add("Ubezpiecznie 2");
        customer2.getPurchases().add("Ubezpiecznie 3");
        customer2.setAccountManager(employee2);
        persons.add(customer2);
        
        // Wyświetlanie modelu
        for (Person person : persons) {
            System.out.println(person.getShortInfo());
        }
    }
}