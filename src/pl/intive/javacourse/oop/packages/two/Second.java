package pl.intive.javacourse.oop.packages.two;

import pl.intive.javacourse.oop.packages.Fourth;
import pl.intive.javacourse.oop.packages.one.First;

class Third {
    
    public static void doSmth() {
        System.out.println("Third -> Coś ");
    }
}

public class Second {

    // By użyć klasy First należy ją zaimportować, gdyż jest w innym pakiecie.
    First first;
    // By użyć klasy Fourth również należy ją zaimportować, gdyż jest w innym pakiecie.
    Fourth forth;
    
    public Second() {
    }

    public First getFirst() {
        return first;
    }

    public void setFirst(First first) {
        this.first = first;
    }
    
    public void doSmth() {
        Third.doSmth();
    }
}
