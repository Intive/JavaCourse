package pl.intive.javacourse.oop.packages;

public class Fourth {

    String str; 
    
    public Fourth(String str) {
        this.str = str;
    }
    
    public String getStr() {
        return str;
    }
}
