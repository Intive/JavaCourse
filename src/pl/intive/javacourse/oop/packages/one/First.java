package pl.intive.javacourse.oop.packages.one;

public class First {
    
    final int num;
    
    public First(int num) {
        this.num = num;
    }
    
    public int getNum() {
        return num;
    }
}