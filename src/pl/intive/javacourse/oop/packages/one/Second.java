package pl.intive.javacourse.oop.packages.one;

// Druga wersja klasy Second, nie ma konfliktu bo jest innym pakiecie
public class Second {

    String str;
    int num;
    
    public Second() {
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
