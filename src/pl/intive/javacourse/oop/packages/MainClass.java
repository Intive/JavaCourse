package pl.intive.javacourse.oop.packages;

import pl.intive.javacourse.oop.packages.two.Second;

public class MainClass {

    public static void main(String[] args) {
        // Można podawać pełną nazwę - nie zalecena jeśli nie jest potrzebne
        pl.intive.javacourse.oop.packages.one.First first = new pl.intive.javacourse.oop.packages.one.First(10);
        // Pole nie jest widoczne spoza pakietu wiec nie ma do niego dostępu
        //first.num;
        // Metoda getNum klasy First jest publiczna więc można ja wywołać
        System.out.println(first.getNum());

        // By użyć klasy Second należy ją zaimportować, gdyż jest w innym pakiecie.
        Second second = new Second();
        second.setFirst(first);
        System.out.println(second.getFirst().getNum());

        // Nie można wywołać metody Third.doSmth() bo klasa nie jest widoczna
        second.doSmth();
        
        Fourth fourth = new Fourth("Jakiś napis");
        // Można się odnieść bezpośrednio do pola - klasa Fourth jest w tym samym pakiecie
        System.out.println(fourth.str);
        // Można się odnieść do metody która jest publiczna 
        System.out.println(fourth.getStr());
    }
}
