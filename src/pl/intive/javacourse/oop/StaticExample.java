package pl.intive.javacourse.oop;


class PointHelper {

    /**
     * Punkt znajdujący się w środku układy współrzędnych.
     */
    // Istnieje jedna instancja do której odwołuje się CENTER - pole statyczne
    static final Point CENTER = new Point(0, 0);
    
    /**
     * Oblicza kwadrat v. 
     * 
     * @param v Liczba.
     * @return Kwadrat liczby v.
     */
    private static int square(int v) {
        return v * v;
    }
    
    /**
     * Oblicza odległość euklidesową pomiędzy punktami p1 i p2. Zwraca zaokrąglony wynik do najbliżej liczby całkowitej.
     * 
     * @param p1 Punkt 1.
     * @param p2 Punkt 2.
     * @return Odległość euklidesowa pomiedzy punktami p1 i p2. 
     */
    static int calcDistance(Point p1, Point p2) {
        // Z metody statycznej można odwołać się do metod statycznych tej samej klasy, do zmiennych lokalnych lub argumetów.
        // Math.sqrt czy Math.round to również metody statyczne
        return (int) Math.round(Math.sqrt(square(p1.x - p2.x) + square(p1.y - p2.y)));
    }

    static int calcDistanceToCenter(Point p) {
        return calcDistance(p, CENTER);
    }
}

class Point {
    // Pola niestatyczne. Istnieją dla każdego obiektu. 
    int x;
    int y;
    
    Point() {
    }
    
    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    /**
     * @return Napis opisujący punkt
     */
    String print() {
        return String.format("(x=%d,y=%d)", x, y);
    }
}


public class StaticExample {
    
    private void printDistInfo(Point p) {
        // Odwołanie do metody statycznej
        int distance = PointHelper.calcDistanceToCenter(p);
        System.out.printf("Odległość z punktu %s do poczatku układu współrzędnych wynosi %d\n", p.print(), distance);
    }

    private void printDistInfo(Point p1, Point p2) {
        // Odwołanie do metody statycznej
        int distance = PointHelper.calcDistance(p1, p2);
        System.out.printf("Odległość z punktu %s do %s wynosi %d\n", p1.print(), p2.print(), distance);
    }

    public static void main(String[] args) {
        Point p1 = new Point();
        // Możemy odwoływać się do niestatycznych pól tylko przez referencje
        p1.x = 3;
        p1.y = 0;
        
        Point p2 = new Point();
        p2.x = 0;
        p2.y = 3;
        
        StaticExample se = new StaticExample();
        se.printDistInfo(p1, p2);
        se.printDistInfo(p1, PointHelper.CENTER);
        se.printDistInfo(p1);
        se.printDistInfo(p2);
    }
}
