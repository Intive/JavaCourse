package pl.intive.javacourse.oop.classes;


// Definicja klasy
/**
 *  Klasa przechowuje podstawowe informacje o osobie.
 */
class Person {
    // Pola klasy
    /** Imię */
    // Domyślna wartość dla referencji do obiektu to null.
    String firstName;
    /** Nazwisko */
    String lastName;
    /** Wiek w latach, rocznikowo. */
    // Domyślna wartość dla typu int to 0
    int age;
    /** Średnie wynagrodzenie. */
    // Domyślna wartość dla typu double to 0.0
    double avgSalary;
    /** Wymiar czasu pracy. 1 -> pełny etat */
    double timeJob;
    // Uwaga - pola avgSalary i timeJob powinny być bardziej precyzyjnego typu. Typ double użyty dla uproszczenia. 
    /** Płeć. M - mężczyzna, K - kobieta */
    // Domyślna wartość dla typu char to 0 (\u0000)
    char sex;

    
    // Metody klasy
    /**
     * Zwiększa wiek o rok.
     */
    void newYear() {
        age++;
    }
    
    /**
     * Aktualizuje warunki pracy - pola średnie miesięczne wynagrodzenie i wymiar czasu pracy - jeśli wartości są poprawne wartość.
     * Wartości są poprawne, jeśli newAvgSalary i newTimeJob są wieksze od 0, lub obydwie wartości są równe 0.
     * 
     * @param newAvgSalary Nowa wartość średniego miesięcznego wynagrodzenia.
     * @param newTimeJob Nowa wartość wymiaru czasu pracy.
     */
    void changeWorkingConditions(double newAvgSalary, double newTimeJob) {
        if (newAvgSalary > 0 && newTimeJob > 0 || newAvgSalary == 0 && newTimeJob == 0) {
            avgSalary = newAvgSalary;
            timeJob = newTimeJob;
        }
    }
    
    String printInfo() {
        return String.format("%s %s, lat: %d, średnia pensja: %.2f PLN, wymiar czasy pracy: %.2f, płeć: %s", firstName, lastName, age, avgSalary, timeJob, (sex == 'M' ? "mężczyzna" : "kobieta"));
    }
    
    String getFullName() {
        return firstName + " " + lastName;
    }
    
    double hourlyRate() {
        return avgSalary > 0 && timeJob > 0 ? avgSalary / (150 * timeJob) : 0; 
    }
}

public class SimpleClass {

    public static void main(String[] args) {
        // Deklarowanie zmienej - referencji do klasy Person 
        Person firstPerson;
        
        // Tworzenie obiektu
        firstPerson = new Person();
        
        // Wypisanie danych osoby, domyślne wartości
        System.out.println(firstPerson.printInfo());
        
        // Ustawienie wartości obiektu
        firstPerson.firstName = "Jan";
        firstPerson.lastName = "Kowalski";
        firstPerson.age = 30;
        firstPerson.avgSalary = 2100;
        firstPerson.timeJob = 1;
        firstPerson.sex = 'M';
        
        // Osoba ma zmienione wartości 
        System.out.println(firstPerson.printInfo());
        
        // Zmiana wartości za pomocą metod
        firstPerson.newYear();
        System.out.printf("Wypracowana stawka na godzinę %s przed podwyżką: %.2f\n", firstPerson.getFullName(), firstPerson.hourlyRate());
        firstPerson.changeWorkingConditions(2200, 1);
        System.out.printf("Wypracowana stawka na godzinę %s po podwyżce: %.2f\n", firstPerson.getFullName(), firstPerson.hourlyRate());
        
        // Osoba ma zmienione wartości 
        System.out.println(firstPerson.printInfo());
        
        //
        // Tworzenie drugiej osoby - drugi obiekt tej samej klasy
        Person secondPerson = new Person();
        secondPerson.firstName = "Anna";
        secondPerson.lastName = "Nowak";
        secondPerson.age = 25;
        secondPerson.avgSalary = 0;
        secondPerson.timeJob = 0;
        secondPerson.sex = 'K';

        // Wypisanie danych drugiej osoby
        System.out.println(secondPerson.printInfo());
        
        // Operacje na polach
        if (firstPerson.age > secondPerson.age) {
            System.out.printf("%s jest starszy(a) od %s\n", firstPerson.getFullName(), secondPerson.getFullName());
        } else if (firstPerson.age < secondPerson.age) {
            System.out.printf("%s jest starszy(a) od %s\n", secondPerson.getFullName(), firstPerson.getFullName());
        } else {
            System.out.printf("%s i %s są w tym samym wieku\n", firstPerson.getFullName(), secondPerson.getFullName());
        }
    }
}
