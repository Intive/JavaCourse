package pl.intive.javacourse.oop.classes;

/**
 *  Klasa przechowuje podstawowe informacje o osobie.
 */
class EncPerson {
    // Statyczne pole klasy
    public static final char MALE = 'M';
    public static final char FEMALE = 'W';
    public static final char UNKNOWN = 'U';
    
    // Pola klasy
    /** Imię */
    private String firstName;
    /** Nazwisko */
    private String lastName;
    /** Wiek w latach, rocznikowo. */
    private int age;
    /** Średnie wynagrodzenie. */
    private double avgSalary;
    /** Płeć. M - mężczyzna, W - kobieta, U - nieznana. */
    /** Wymiar czasu pracy. 1 -> pełny etat */
    double timeJob;
    // Inicjowanie jawne pola przy deklaracji
    private char sex = UNKNOWN;
    
    // Konstruktory
    /**
     * Tworzy obiekt z domyślnymi (pustymi) wartościami.
     */
    public EncPerson() {
        // Inicjowanie wartości w kontruktorze
        firstName = "";
        lastName = "";
    }
    
    /**
     * Ustawia wartości 
     * 
     * @param firstName Imię. Musi być różne od null.
     * @param lastName Nazwisko. Musi być róże od null.
     * @param age Wiek. Musi być >= 0.
     * @param avgSalary Średnia pensja. Musi być >= 0.
     * @param sex Płeć. Musi posiadać 
     */
    public EncPerson(String firstName, String lastName, int age, double avgSalary, char sex) {
        // do pól klasy możemy odnieść sie korzystając ze słowa kluczowego this
        this.firstName = firstName != null ? firstName : null;
        this.lastName = lastName != null ? lastName : null;
        this.age = age >= 0 ? age : 0;
        this.avgSalary = avgSalary >= 0 ? avgSalary : 0;
        setSex(sex);
    }
    
    
    // Metody get i set
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName != null) {
            this.firstName = firstName;
        }
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName != null) {
            this.lastName = lastName;
        }
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
        }
    }

    public double getAvgSalary() {
        return avgSalary;
    }

    public void setAvgSalary(double avgSalary) {
        if (avgSalary >= 0) {
            this.avgSalary = avgSalary;
        }
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        if (sex == MALE || sex == FEMALE || sex == UNKNOWN) {
            this.sex = sex;
        }
    }
    

    // Pozostałe metody klasy
    /**
     * Zwiększa wiek o rok.
     */
    public void newYear() {
        age++;
    }

    /**
     * Aktualizuje warunki pracy - pola średnie miesięczne wynagrodzenie i wymiar czasu pracy - jeśli wartości są poprawne wartość.
     * Wartości są poprawne, jeśli newAvgSalary i newTimeJob są wieksze od 0, lub obydwie wartości są równe 0.
     * 
     * @param newAvgSalary Nowa wartość średniego miesięcznego wynagrodzenia.
     * @param newTimeJob Nowa wartość wymiaru czasu pracy.
     */
    void changeWorkingConditions(double newAvgSalary, double newTimeJob) {
        if (newAvgSalary > 0 && newTimeJob > 0 || newAvgSalary == 0 && newTimeJob == 0) {
            avgSalary = newAvgSalary;
            timeJob = newTimeJob;
        }
    }

    public String printInfo() {
        return String.format("%s %s, lat: %d, średnia pensja: %.2f PLN, płeć: %s", firstName, lastName, age, avgSalary, (sex == 'M' ? "mężczyzna" : "kobieta"));
    }
    
    public String getFullName() {
        return firstName + " " + lastName;
    }
    
    double hourlyRate() {
        return avgSalary > 0 && timeJob > 0 ? avgSalary / (150 * timeJob) : 0; 
    }
}

public class Ecnapsulation {

    public static void main(String[] args) {
        // Deklarowanie zmienej - referencji do klasy Person 
        EncPerson firstPerson;
        
        //
        // Tworzenie obiektu dla pierwszej osoby
        firstPerson = new EncPerson();
        
        // Wypisanie danych osoby, domyślne wartości
        System.out.println(firstPerson.printInfo());
        
        // Ustawienie wartości obiektu
        // Nie możnna bezpośrednio odnosić się do prywatnego pola
        // firstPerson.firstName = "Jan";
        // Należy ustawić używając metod setXxx
        firstPerson.setFirstName("Jan");
        firstPerson.setLastName("Kowalski");
        firstPerson.setAge(30);
        firstPerson.setAvgSalary(2100);
        firstPerson.setSex(EncPerson.MALE);
        
        // Osoba ma ustawione wartości 
        System.out.println(firstPerson.printInfo());
        
        // Zmiana wartości za pomocą metod
        firstPerson.newYear();
        System.out.printf("Wypracowana stawka na godzinę %s przed podwyżką: %.2f\n", firstPerson.getFullName(), firstPerson.hourlyRate());
        firstPerson.changeWorkingConditions(2200, 1);
        System.out.printf("Wypracowana stawka na godzinę %s po podwyżce: %.2f\n", firstPerson.getFullName(), firstPerson.hourlyRate());
        
        // Osoba ma zmienione wartości 
        System.out.println(firstPerson.printInfo());
        
        //
        // Tworzenie obiektu dla drugiej osoby - za pomocą konstruktora
        EncPerson secondPerson = new EncPerson("Anna", "Nowak", 25, 0, EncPerson.FEMALE);

        // Wypisanie danych drugiej osoby
        System.out.println(secondPerson.printInfo());

        // próby ustawienie niepoprawnych wartości
        secondPerson.setAge(-10);
        secondPerson.setAvgSalary(-100d);
        secondPerson.setSex('A');

        // Wartości pól obiektu nie zostały zmienione
        System.out.println(secondPerson.printInfo());
        
        //
        // Operacje na polach
        // Należy użyć metod get
        if (firstPerson.getAge() > secondPerson.getAge()) {
            System.out.printf("%s jest starszy(a) od %s\n", firstPerson.getFullName(), secondPerson.getFullName());
        } else if (firstPerson.getAge() < secondPerson.getAge()) {
            System.out.printf("%s jest starszy(a) od %s\n", secondPerson.getFullName(), firstPerson.getFullName());
        } else {
            System.out.printf("%s i %s są w tym samym wieku\n", firstPerson.getFullName(), secondPerson.getFullName());
        }
    }
}