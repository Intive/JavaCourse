package pl.intive.javacourse.string;

public class StringExample {

	public static void main(String[] args) {

		String str = "To jest łańcuch znakowy.";

		System.out.println(str);

		String contatenated = "Łańcuchy mozna łączyć " + " za pomocą znaku + .";

		System.out.println(contatenated);

		System.out.println("Łańcuchy można łączyc również z innymi zmiennymi: 5 + 6 = " + (5 + 6));
		System.out.println("Łańcuchy można łączyc również z innymi zmiennymi: 5 + 6 = " + 5 + 6);

		System.out.println("Znaki specjalne:");
		System.out.println("- nowa linia: \\n ");
		char ch = '\n';

		System.out.println("- tabulacja: \\t ");
		ch = '\t';
		
		System.out.println("- cudzysłów: \"");
		ch = '"';
		ch = '\'';
		
		System.out.println("- backslash: \\");
		ch = '\\'; 

		System.out.println("Pierwsza linia\nDruga linia\n\tTrzecia linia " + ch);
	}
}