package pl.intive.javacourse.string;

public class CharArray {

	public static void main(String[] args) {

		String str = "Jakiś tekst.";

		System.out.println(str);

		char[] a = str.toCharArray();
		
		String str2 = new String(a);
		System.out.println(str2);
	}
}
