package pl.intive.javacourse.string;

/**
 * Przykład użycia String.format. Więcej przykladów na
 * https://examples.javacodegeeks.com/core-java/lang/string/java-string-format-
 * example/
 *
 */
public class StringFormatExample {

	public static void main(String[] args) {

		double decimal = 5.5555;
		int number = 100;

		String output = String.format("Dwa miejsca po przecinku: %.2f", decimal);
		output += "\n";
		output += String.format("Przed liczbą zostaną dodane 0, tak żeby łączna długośc wynosiła 6: %06d", number);

		System.out.println(output);

	}
}
